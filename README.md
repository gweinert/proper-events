![IdeaWork](http://www.ideawork.com/images/ideawork-studios-logo.png)

# IdeaWork Front-end Boilerplate

This is the new front-end boilerplate for all IdeaWork Studios projects. The purpose of this boilerplate is to create a development experience that is seamless, predictable, and consistent across all projects during the initial development process and when we have to perform routine maintenance. If you have any questions or concerns, please create an issue for this repo clearly outlining your inquiry. Thanks.

## Requirements
The following are the package requirements for initial installation of the boilerplate:

* [`node v4.0.0`](https://nodejs.org/en/)
```
sudo npm cache clean -f
sudo npm install -g n
sudo n 4.0.0
```

* [`npm v3.3.10`](https://www.npmjs.com/)
```
sudo npm install npm@3.3.10 -g
```

* [`webpack >= v1.12.2`](http://webpack.github.io/docs/)
```
npm install -g webpack
```

* [`webpack-dev-server >= v1.12.0`](http://webpack.github.io/docs/webpack-dev-server.html)
```
npm install -g webpack-dev-server
```

## Getting Started
1. Create a new directory.
2. Copy the files from this repo into the directory you just created.
3. Run the following command: `npm run init`
4. Answer the questions on the command line.
5. Start developing!

## Build Scripts

* `npm run build:dev`: This command uses webpack to build the dev instance of your project located at `/dist/dev`. This directory will eventually have the capability to be pushed to a unique Github project page. For right now, you don't really need to worry about this command.
* `npm run build:local`: This command uses webpack to build the local instance of your project located at `/dist/local`. This directory also serves as the content base for the server instance that is created by Webpack Dev Server.
* `npm run build:release`: This command uses webpack to build the release instance of your project located at `/dist/release`. These files are compressed, minified, and are appended with a unique hash to prevent any potential caching issues.
* `npm run init`: This command installs all of your project's dependencies, asks you project specific questions, uses Webpack to build your local project files, and starts a server instance using Webpack Dev Server.
* `npm run serve`: This command starts a local server instance using Webpack Dev server.

## Things to Know

#### Alt
Alt is the flux library this boilerplate uses to manage application state. Alt has clear and thorough documentation and is extremely user friendly. I would suggest reading Alt's documentation at the following location: [`http://alt.js.org/`](http://alt.js.org/).

#### Actions
All actions that you need to define in your app, should be placed witin the following directory: `/src/app/scripts/actions`. You can use `ContentActions.js` as a template for building your actions; however, I would suggest reading Alt's documentation at the following location: [`http://alt.js.org/guide/actions/`](http://alt.js.org/guide/actions/).

#### ES2015 (ES6)
Foundationally, this boilerplate has been built with ES2015, which is the new version of Javascript that has been ratified by the W3C. We use a transpiler (Babel), to convert our ES2015 (ES6) code into ES5 compliant code. This way, we have the ability to use the features of tomorrow today. The new features that ES2015 (ES6) brings enhances the development experience and makes many development tasks much easier. For reference, you can visit [`https://babeljs.io/docs/learn-es2015/`](https://babeljs.io/docs/learn-es2015/).

#### Global Objects
All global objects need to be defined by the Webpack Provide Plugin, which is located in `/webpack/config/plugins.js` at the bottom of the file. As you can see, jQuery, Lodash, React, and ReactDOM are already defined for global use within your app. This way you do not always need to require them when you use them. Keep in mind that Webpack will only include these in your bundle if you reference them somewhere in your files. With that being said, if you do not need jQuery or Lodash, don't use them. Your output file will be much smaller, which will give your app a performance boost.

#### Global Variables
All global variables should be defined in `package.json` under the `config` object. This is where Webpack looks to expose the variables for use within the app. At any time during the development process, you may reference these variables in your code. They are not set globally on the window object, however. This way we do not pollute the global scope nor do we need to define anything within `index.jade`.

#### Hot Module Replacement
Hot module replacement works by watching changes to files, and signaling the browser to replace specific modules, or functions, but not reload the entire page. The major benefit of hot module replacement is that state is not lost – if you’ve had to perform a number of actions to get to a specific state, you won’t perform a full page refresh and lose the history of those actions.

#### Pages
All page templates should be added to the object contained within the following file: `/src/app/scripts/components/pages.js`. The key should be identitical to the template name contained within the CMS. The value should be the following the page component associated with with said template name. Each page is passed a `contentArea` object via `this.props`, which contains all the data needed for building your view. `className` is also passed down via `this.props`, which contains a page, path, and template name class. When creating a new page component, you should follow the example within the `Home.js` component. The important things to do are import the `Page.js` component and export your new page component in the following manner: `export default Page(NewPageComponent);`

#### React
React is a javascript library for building user interfaces. React forms the backbone of this boilerplate, allowing all web apps using this boilerplate to leverage the power of React to build their UI. I would suggest reading Reacts's documentation at the following location: [`https://facebook.github.io/react/`](https://facebook.github.io/react/).

#### Routing

#### Sources
All sources that you need to define in your app, should be placed witin the following directory: `/src/app/scripts/sources`. Sources are essentially classes that are set up with static methods that allow you to communicate with an API source. Most times, you will not need to add an additional source; however, if you need to communicate with multiple APIs, then you can add an additional source file in the aforentioned directory. These source classes are generally used within action files to perform asynchronous actions.

#### Stores
All stores that you need to define in your app, should be placed witin the following directory: `/src/app/scripts/stores`. You can use `ContentStore.js` as a template for building your store; however, I would suggest reading Alt's documentation at the following location: [`http://alt.js.org/guide/store/`](http://alt.js.org/guide/store/).

## Debugging Tools

#### Alt Dev Tools
Alt Dev Tools is a Chrome extension that allows you to inspect your Alt stores and actions. It gives you the ability to view the current state of your stores as well as a list of all the actions that have been dispatched since the point at which your app has been initialized. Download it here: [`Alt Dev Tools`](https://chrome.google.com/webstore/detail/alt-devtools/jfjefoacbhfenehankalijibcccajchg?hl=en)

#### React Dev Tools
React Dev Tools is a Chrome extension that allows you to inspect the React component hierarchies in the Chrome Developer Tools. You will get new tab called React in your Chrome DevTools. This shows you the root React components that was rendered on the page, as well as the subcomponents that they ended up rendering. Download it here: [`React Dev Tools`](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en)

## Dependencies
####Project Dependencies
* `alt` ([`github`](https://github.com/goatslacker/alt), [`npm`](https://www.npmjs.com/package/alt), [`web`](http://alt.js.org/)) : Alt is the flux library this boilerplate uses to manage application state. Alt has clear and thorough documentation and is extremely user friendly.
* `axios` ([`github`](https://github.com/mzabriskie/axios), [`npm`](https://www.npmjs.com/package/axios)) : This boilerplate uses Axios, a promise-based HTTP library, to interact with our backend API.
* `babel-polyfill` ([`github`](https://github.com/babel/babel), [`npm`](https://www.npmjs.com/package/babel-polyfill), [`web`](https://babeljs.io/docs/usage/polyfill/)): This package provides polyfills for features that are currently available in ES2015, but are not supported in all modern browsers, e.g. the `Promise` object.
* `history` ([`github`](https://github.com/rackt/history), [`npm`](https://www.npmjs.com/package/history)): This package works alongside React Router to polyfill the History API. This is necessary for properly updating the state of the browser location.
* `jquery` ([`github`](https://github.com/jquery/jquery), [`npm`](https://www.npmjs.com/package/jquery), [`web`](https://jquery.com/)): This is available globally within your app using `$`. Webpack will only load jQuery into your compiled bundle if you use it; otherwise, it will be excluded from the final compiled bundle.
* `lodash` ([`github`](https://github.com/lodash/lodash), [`npm`](https://www.npmjs.com/package/lodash), [`web`](https://lodash.com/)): Lodash is a javascript utility library delivering consistency, modularity, performance, & extras. This is available globally within your app using `_`. Webpack will only load Lodash into your compiled bundle if you use it; otherwise, it will be excluded from the final compiled bundle.
* `react` ([`github`](https://github.com/facebook/react), [`npm`](https://www.npmjs.com/package/react), [`web`](https://facebook.github.io/react/)): React is a javascript library for building user interfaces. React forms the backbone of this boilerplate, allowing all web apps using this boilerplate to leverage the power of React to build their UI.
* `react-addons-css-transition-group` ([`github`](https://github.com/facebook/react), [`npm`](https://www.npmjs.com/package/react-addons), [`web`](https://facebook.github.io/react/docs/animation.html)): This package is a React Addons component that adds classes to page elements used for page transitions. You can customize your page transitions by modifying `_page-transitions.scss`.
* `react-dom` ([`github`](https://github.com/facebook/react), [`npm`](https://www.npmjs.com/package/react-dom), [`web`](https://facebook.github.io/react/docs/top-level-api.html)): The ReactDOM package provides DOM-specific methods that can be used at the top level of your app and as an escape hatch to get outside of the React model if you need to. Most of your components should not need to use this module.
* `react-router` ([`github`](https://github.com/rackt/react-router), [`npm`](https://www.npmjs.com/package/react-router)): React Router is a complete routing library for React. It keeps your UI in sync with the URL. It has a simple API with powerful features like lazy code loading, dynamic route matching, and location transition handling built in.

####Development Dependencies
* `autoprefixer-loader` ([`github`](https://github.com/passy/autoprefixer-loader), [`npm`](https://www.npmjs.com/package/autoprefixer-loader)): This package automagically adds vendor prefixes to your css styles.
* `babel-core` ([`github`](https://github.com/babel/babel), [`npm`](https://www.npmjs.com/package/babel-core), [`web`](https://babeljs.io/)): Babel is used for transpiling your ES2015 (ES6) code into ES5 compliant code, which all modern browsers recognize.
* `babel-loader` ([`github`](https://github.com/babel/babel-loader), [`npm`](https://www.npmjs.com/package/babel-loader), [`web`](https://babeljs.io/docs/setup/#webpack)): This package allows transpiling javascript files using Babel and Webpack.
* `babel-preset-es2015` ([`github`](https://github.com/babel/babel), [`npm`](https://www.npmjs.com/package/babel-preset-es2015), [`web`](https://babeljs.io/docs/plugins/preset-es2015/)): Out of the box Babel doesn't do anything. In order to actually do anything to your code you need to enable presets. Presets are sharable configs that add functionality to Babel.
* `babel-preset-react` ([`github`](https://github.com/babel/babel), [`npm`](https://www.npmjs.com/package/babel-preset-react), [`web`](http://babeljs.io/docs/plugins/preset-react/)): Out of the box Babel doesn't do anything. In order to actually do anything to your code you need to enable presets. Presets are sharable configs that add functionality to Babel.
* `babel-preset-stage-0` ([`github`](https://github.com/babel/babel), [`npm`](https://www.npmjs.com/package/babel-preset-stage-0), [`web`](http://babeljs.io/docs/plugins/preset-stage-0/)): Out of the box Babel doesn't do anything. In order to actually do anything to your code you need to enable presets. Presets are sharable configs that add functionality to Babel.
* `css-loader` ([`github`](https://github.com/webpack/css-loader), [`npm`](https://www.npmjs.com/package/css-loader)): Webpack uses this to properly load css.
* `extract-text-webpack-plugin` ([`github`](https://github.com/webpack/extract-text-webpack-plugin), [`npm`](https://www.npmjs.com/package/extract-text-webpack-plugin)): It moves every `require("style.css")` in entry chunks into a separate css output file. So your styles are no longer inlined into the javascript. If your total stylesheet volume is big, it will be faster because the stylesheet bundle is loaded in parallel to the javascript bundle.
* `file-loader` ([`github`](https://github.com/webpack/file-loader), [`npm`](https://www.npmjs.com/package/file-loader)): Webpack uses this to handle loading files properly, e.g. font files.
* `image-webpack-loader` ([`github`](https://github.com/tcoopman/image-webpack-loader), [`npm`](https://www.npmjs.com/package/image-webpack-loader)): Webpack uses this to handle loading image files properly.
* `jade` ([`github`](https://github.com/jadejs/jade), [`npm`](https://www.npmjs.com/package/jade), [`web`](http://jade-lang.com/)): Jade is a high performance template engine heavily influenced by HAML and implemented with javascript for Node and browsers.
* `jade-html-loader` ([`github`](https://github.com/bline/jade-html-loader), [`npm`](https://www.npmjs.com/package/jade-html-loader)): Webpack uses this loader to compile jade files into readable html.
* `json-loader` ([`github`](https://github.com/webpack/json-loader), [`npm`](https://www.npmjs.com/package/json-loader)): Webpack uses this loader for properly handling json files.
* `minimist` ([`github`](https://github.com/substack/minimist), [`npm`](https://www.npmjs.com/package/minimist)): Formats command line arguments into an accessible javascript object.
* `node-sass` ([`github`](https://github.com/sass/node-sass), [`npm`](https://www.npmjs.com/package/node-sass)): Node-sass is a library that provides binding for Node.js to libsass, the C version of the popular stylesheet preprocessor, Sass.
* `open-browser-webpack-plugin` ([`github`](https://github.com/baldore/open-browser-webpack-plugin), [`npm`](https://www.npmjs.com/package/open-browser-webpack-plugin)): Opens a new browser tab when Webpack loads.
* `react-hot-loader` ([`github`](https://github.com/gaearon/react-hot-loader), [`npm`](https://www.npmjs.com/package/react-hot-loader)):  This package allows us to use hot reloading on React components.
* `readline-sync` ([`github`](https://github.com/anseki/readline-sync), [`npm`](https://www.npmjs.com/package/readline-sync)): Readline-sync lets your script have a conversation with the user via a console. When a user is prompted with and answers a question, Readline-sync returns the answer, which allows you to assign it to a variable.
* `rimraf` ([`github`](https://github.com/isaacs/rimraf), [`npm`](https://www.npmjs.com/package/rimraf)): This package allows you to remove files and directories from your computer's file system.
* `sass-loader` ([`github`](https://github.com/jtangelder/sass-loader), [`npm`](https://www.npmjs.com/package/sass-loader)): Webpack uses this loader to properly handle Sass files.
* `style-loader` ([`github`](https://github.com/webpack/style-loader), [`npm`](https://github.com/webpack/style-loader)): Webpack uses this loader to properly load styles into your app.
* `webpack` ([`github`](https://github.com/webpack/webpack), [`npm`](https://www.npmjs.com/package/webpack), [`web`](https://webpack.github.io/docs/)): Webpack is a module bundler. It takes modules with dependencies and generates static assets representing those modules.
* `webpack-dev-server` ([`github`](https://github.com/webpack/webpack-dev-server), [`npm`](https://www.npmjs.com/package/webpack-dev-server), [`web`](https://webpack.github.io/docs/webpack-dev-server.html)): The Webpack Dev Server is a little Node.js Express server, which uses the Webpack Dev Middleware to serve a Webpack bundle.
* `webpack-path-rewriter` ([`github`](https://github.com/skozin/webpack-path-rewriter), [`npm`](https://www.npmjs.com/package/webpack-path-rewriter)): This is a Webpack plugin that writes text resources to the file system and replaces selected paths with their public counterparts.

## Project Files
* [`package.json`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/package.json): This is where all your project metadata is located.
* [`prompts.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/prompts.js): This file is used upon the initial project build for gathering project specific information, e.g. the API and Content Directory paths.
* [`/src/index.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/src/index.js): This is the Webpack entry point of the boilerplate. Your Jade, Javascript, and Sass files are required here and then output to their respective `/dist` folder.
* [`/src/app/markup/index.jade`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/src/app/markup/index.jade): This serves as the main structural container for your app where all your app assets are referenced and defined.
* [`/src/app/scripts/main.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/src/app/scripts/main.js): This file takes care of building your app's routes as well as intializing the app itself.
* [`/src/app/scripts/actions/ContentActions.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/src/app/scripts/actions/ContentActions.js): This file defines all the actions that pertain to the Content Store. This is also where you can find your asynchronous actions, which retrieve data from out backend API.
* [`/src/app/scripts/alt/alt.js`](https://github.com/SMLMRKHLMS/iws-frontend/tree/master/src/app/scripts/alt/alt.js): This instantiates out Alt instance, which can then be required and used in other files.
* [`/src/app/scripts/components/pages.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/src/app/scripts/components/pages.js): This file maps template names, which are defined within the CMS, to their location within the app. This is where all page components must be defined.
* [`/src/app/scripts/components/App/App.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/src/app/scripts/components/App/App.js): This is the main container view component. This is where our application state is held, which is then passed down to its children via props.
* [`/src/app/scripts/components/Default/Default.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/src/app/scripts/components/Default/Default.js): This is the default view component, which is used as a fallback when a route's template is not explicitly defined.
* [`/src/app/scripts/components/Footer/Footer.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/src/app/scripts/components/Footer/Footer.js): This file is a global view component that is passed a `nav` object as its props. This `nav` object contains all the content areas whose parent has the path of `/footer`.
* [`/src/app/scripts/components/Form/Form.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/src/app/scripts/components/Form/Form.js): This is a form wrapper component, which tries to simplify and abstract away the diffculty and nuances of building forms.
* [`/src/app/scripts/components/Header/Header.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/src/app/scripts/components/Header/Header.js): This files is a global view component that is passed a `nav` object as its props. This `nav` object contains all the content areas whose parent has the id of `0`.
* [`/src/app/scripts/components/Home/Home.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/src/app/scripts/components/Home/Home.js): This is the home page view component, which is shown at the root of our app.
* [`/src/app/scripts/components/Page/Page.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/src/app/scripts/components/Page/Page.js): This component should be inherited by all page components. It specifies page specific prop types and default props. Any logic that should be applied to all page view components should be defined here.
* [`/src/app/scripts/sources/API.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/src/app/scripts/sources/API.js): This file is where axios is leveraged to to build our API endpoints, which our asynchronous actions use.
* [`/src/app/scripts/stores/ContentStore.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/src/app/scripts/stores/ContentStore.js): This flux store is where all of our content is held and persisted. We no longer have to make three separate stores for the nav, content areas, and content slides. The Content Store automatically sorts and adds content slides to its associated content area parent, which also holds all the information necessary for build its associated route.
* [`/src/app/styles/_normalize.scss`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/src/app/styles/_normalize.scss): This file normalizes all base styles across browsers.
* [`/src/app/styles/_page-transition.scss`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/src/app/styles/_page-transition.scss): This file holds all the styles used for styling your page transitions.
* [`/src/app/styles/main.scss`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/src/app/styles/main.scss): This is the main entry point for all of your app's styles.
* [`/webpack/webpack.config.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/webpack/webpack.config.js): This is the main Webpack config file used for building your app's assets.
* [`/webpack/config/debug-loaders.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/webpack/config/debug-loaders.js): These loaders are used when your environment is either `dev` or `local`.
* [`/webpack/config/dev-server.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/webpack/config/dev-server.js): This is where the configuration is defined for our local Webpack Dev Server instance.
* [`/webpack/config/plugins.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/webpack/config/plugins.js): This file defines the plugins webpack should use to make enhancements and optimize the build process.
* [`/webpack/config/release-loaders.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/webpack/config/release-loaders.js): These loaders are used when your environment is `release`.
* [`/webpack/environments/dev.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/webpack/environments/dev.js): This is the build that will potentially be used for deploying our app to a Github Pages repo.
* [`/webpack/environments/local.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/webpack/environments/local.js): This is the build used for local development and debugging.
* [`/webpack/environments/release.js`](https://github.com/SMLMRKHLMS/iws-frontend/blob/master/webpack/environments/release.js): This is the build used for production ready code.
