/*----------------------------------------------------------------------------*\
 TITLE: API.JS
 DESCRIPTION: THIS FILE TAKES CARE OF SETTING UP ALL THE API ENDPOINTS FOR THE
              APP.
 AUTHOR: MARK HOLMES
 NOTES: TRADITIONALLY, THIS FILE WOULD BE INCLUDED IN AN ACTIONS FILE TO BE USED
        FOR DATA FETCHING AND/OR MANIPULATION. LOOK AT NAVACTIONS.JS OR
        CONTENTAREAACTIONS.JS FOR AN EXAMPLE.
\*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*\
 DEPENDENCIES
\*----------------------------------------------------------------------------*/

import axios from 'axios';

/*----------------------------------------------------------------------------*\
 CLASS DEFINITION
\*----------------------------------------------------------------------------*/

export default class API {
  static get(endpoint, query = {}) {
    return new Promise((resolve, reject) => {
      axios.get(`${API_PATH}${endpoint}`, {
        params: query
      }).then((response) => {
        resolve(response.data);
      }).catch((error) => {
        console.dir(error);
        reject(error);
      });
    });
  }
  static post(endpoint, data) {
    return new Promise((resolve, reject) => {
      axios.post(`${API_PATH}${endpoint}`, data).then((response) => {
        resolve(response.data);
      }).catch((error) => {
        reject(error);
      });
    });
  }
};
