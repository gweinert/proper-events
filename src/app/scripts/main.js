/*----------------------------------------------------------------------------*\
 TITLE: MAIN.JS
 DESCRIPTION: THIS FILE TAKES CARE OF BUILDING ROUTES AND INITIALIZING THE APP.
 AUTHOR: MARK HOLMES
 NOTES:
\*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*\
 DEPENDENCIES
\*----------------------------------------------------------------------------*/

import App from './components/App/App.js';
import { createHistory } from 'history'
import ContentActions from './actions/ContentActions.js';
import pages from './components/pages.js';
import { IndexRoute, Redirect, Route, Router } from 'react-router';

/*----------------------------------------------------------------------------*\
 CLASS DEFINITION
\*----------------------------------------------------------------------------*/

class AppInitializer {
  constructor() {
    this.state = {
      mountPoint: '.wrapper',
      contentAreas: []
    };
  }
  buildRoutes() {
    return this.state.contentAreas.map((route, i) => {
      let component = pages[route.TEMPLATE] || pages['default'];
      return (
        <Route
          component={ component }
          key={ route.ID }
          path={ route.PATH }/>
      );
    });
  }
  run() {
    ContentActions.get((response) => {
      this.state.contentAreas = response.contentAreas;
      ReactDOM.render((
        <Router history={ createHistory() }>
          <Route path="/" component={ App } >
            <IndexRoute component={ pages['home'] } />
            { this.buildRoutes() }
            <Redirect from="*" to="/" />
            
          </Route>
        </Router>
      ), document.querySelector(this.state.mountPoint));
    });
  }
}

new AppInitializer().run();
