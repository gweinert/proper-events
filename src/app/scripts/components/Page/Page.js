

export default (Component) => {
  return class Page extends React.Component {
    static defaultProps = {
      className: "",
      contentArea: {},
      history: {},
      location: {},
      params: {},
      route: {},
      routeParams: {},
      routes: []
    }
    static propTypes = {
      className: React.PropTypes.string,
      contentArea: React.PropTypes.object,
      history: React.PropTypes.object,
      location: React.PropTypes.object,
      params: React.PropTypes.object,
      route: React.PropTypes.object,
      routeParams: React.PropTypes.object,
      routes: React.PropTypes.array
    }
    constructor(props) { super(props); }
    
    render() {
      // console.log(this.props)
     
     return( 

          <Component { ...this.props } />

      ) 
   }
  
  }
};
