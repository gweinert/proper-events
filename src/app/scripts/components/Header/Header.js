import PageStore from '../../stores/PageStore.js';
import PageActions from '../../actions/PageActions.js';
import CSSTransitionGroup from 'react-addons-css-transition-group';
import ContentStore from './../../stores/ContentStore';
import { Router, Route, Link, browserHistory,  } from 'react-router'
import ProperLogo from '../../../images/proper-logo.png';

export default class Header extends React.Component {
  static propTypes = { 
    myNav: React.PropTypes.array 
  }
  
  constructor(props) { 
	
  	super(props);

  }

  

  state = PageStore.getState()

  componentDidMount = (e) => {

  	PageStore.listen(this.onChange);
  	PageActions.setNavWidth($(React.findDOMNode(this.refs.header)).width());
  	window.addEventListener('resize', this.handleResize);
  	if(window.innerWidth < 820){
  		this.hide()
  	} else {
      // this.show();
      window.setTimeout( () => {
        this.show()
      })
    }

    

  }


    // checks to see if state has updated to handle sublink visibility
  componentDidUpdate(prevProps, prevState){

    var parentActiveLink = document.getElementById(this.state.activeLink)
    if( parentActiveLink){
      if(prevState.activeLink){
        if(prevState.activeLink != this.state.activeLink){
          document.getElementById(prevState.activeLink).setAttribute("class", "")

        }
      }
       parentActiveLink.setAttribute("class", "active-link")
      

    } 

  	if(this.props.activeLink != prevProps.activeLink){

  		if(prevProps.activeLink != this.props.activeLink){
	  		var oldEl = document.getElementById(prevProps.activeLink)

	  		if(oldEl ){
			  	oldEl.setAttribute("class", "")
        
			  }
		  }

	  	var el = document.getElementById(this.props.activeLink)
	  	if(el){
		  	el.setAttribute("class", "active-link")
	  	}
	  }

	  if(prevState.keepSubNavOpen == true && this.state.keepSubNavOpen == false){

	  	this.handleMouseOut();
	  }
  }



  onChange = (state) => {

  	if( this.isEquivalent(this.state, state) ){}
		else{
			this.setState(state);
		}
  	
  }



  handleResize =() =>{
  	if(window.innerWidth < 820){
  		var nav = document.getElementById('mobile-nav')
  		nav.style.width = window.innerWidth + 'px'
  	} else {
        this.hide()

    }
  }

  isEquivalent(a, b) {
    // Create arrays of property names
    var aProps = Object.getOwnPropertyNames(a);
    var bProps = Object.getOwnPropertyNames(b);

    // If number of properties is different,
    // objects are not equivalent
    if (aProps.length != bProps.length) {
        return false;
    }

    for (var i = 0; i < aProps.length; i++) {
        var propName = aProps[i];

        // If values of same property are not equal,
        // objects are not equivalent
        if (a[propName] !== b[propName]) {
            return false;
        }
    }

    // If we made it this far, objects
    // are considered equivalent
    return true;
}

  hide =(e) =>{

  	PageActions.setNavOpen(false)

    PageActions.setAnimate(true)
    
    window.setTimeout( function() {
      PageActions.setAnimate(false)
    }, 700)
  }

  show = (e) => {
  	PageActions.setNavOpen(true)
    PageActions.setAnimate(true)
    
    window.setTimeout( function() {
      PageActions.setAnimate(false)
    }, 700)
  }

  handleContactClick = () => {
    this.openContactModal()
  }

  openContactModal = () => {

    PageActions.setModalShow(true);
  }

  isMobile = () => {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
  }



  render() {
 	var slideDirection = this.state.navOpen ? "left" : "right"
    var showClass = this.state.navOpen ? "hide-arrow" : "show-arrow"


    return (
    	
    	<CSSTransitionGroup transitionName={"header-slide-"+slideDirection} transitionEnterTimeout={700} transitionLeaveTimeout={700}>
    	<div  ref="header" key={this.state.navOpen} className={this.state.navOpen ? "header open" : "header close"} >

    		

    		<div className={"nav-arrow "+ showClass} onClick = {this.show} >
    			<img src="/dist/release/nav-arrow.png" />
			</div>

    		<div className="header-content">
    		
    			<div className="close-button-container">
		    		<div onClick={ this.hide } className="close-button">
		    			<div className="left-x">
			    			<div className="right-x"></div>
		    			</div>
		    		</div> 
	    		</div>
	    		
	    		<div className="header-img">
	    			<Link to={this.props.subSitePath}><img src={ProperLogo} /></Link>
	    		</div>

	    		<div className="nav-list-container no-flick ">
	    		
	    				{this.renderNavLinks()}
					
				</div>

				<div className="nav-footer no-flick ">
					
					<h2><a href="http://properresidence.com/" target="_blank">PROPERRESIDENCE.COM</a></h2>
					<div className="footer-links">

            <h2 onClick={this.handleContactClick}>CONTACT</h2>
						
						<h2 onClick={this.openLegal}>LEGAL</h2>
					</div>
					
					<div className="nav-footer-address">
						<p><a href="https://goo.gl/maps/9Z4pozGcwM12" target="_blank"> 6255 W. SUNSET BLVD LOS ANGELES, CA 93208</a></p>
					</div>

					{this.renderPhoneLink()}

					

          { this.renderSocialLinks() }
				</div>

	    	</div>
    	</div>
    	</CSSTransitionGroup>
  
	);
  }


  renderPhoneLink(){
    if(this.isMobile){
      return(
        <p className="footer-phone"><a href="tel:3105254367">310.525.4367</a></p>
      )
    } else {
      return (
        <p className="footer-phone">310.525.4367</p>
      )
    }
  }

  renderSocialLinks = (e) => {

    
    var socialContent = this.props.socialContent




    return (
      <div className="social-links">
        
        { socialContent.SOCIAL_ICON1 != "" ? <div className="social-icon facebook"><a href={socialContent.SOCIAL_ICON1} target="_blank"> <img src="/dist/release/facebook.png"/></a></div> : ""
          
          }

        { socialContent.SOCIAL_ICON2 != "" ? <div className="social-icon twitter"><a href={socialContent.SOCIAL_ICON2} target="_blank"> <img src="/dist/release/twitter.png"/></a></div> : ""
        
          }

        { socialContent.SOCIAL_ICON3 != "" ? <div className="social-icon pinterest"><a href={socialContent.SOCIAL_ICON3} target="_blank"> <img src="/dist/release/pinterest.png"/></a></div> : ""

          }

        { socialContent.H3 != "" ? <div className="social-icon insta"><a href={socialContent.H3} target="_blank"> <img src="/dist/release/instagram.png"/></a></div> : null 
    
        }
      </div>
    )
  }

  renderNavLinks() {
  	var navLinks = [];
  	var subNav = [];
  	var handleSublink;
  	if(this.props.nav){
  		// this.props.myNav.map(link => { navLinks.push(link) })
	    this.props.nav.map(link => { 
	    	
	    	if(link.SHOWINNAV){
	    		navLinks.push( this.handleSubLinks(link)) 
	    	}
	    });

	    return (
    		<div className="nav-list">
		    	{ navLinks.map(link => { 

            if(link.SHOWINNAV != 0){
  		    		return( 
  		    			<div className="link-container" id={"link-container-"+link.ID} key={link.ID}
  		    				onClick={  link.subpages ? this.handleMouseOver : null  }
                  onTouchEnd ={ link.subpages ? this.handleMouseOver : null }
  		    			 >
  		    				<h2 >
  		    					<Link className="link-title" id={link.TEMPLATE == "redirect" ? 'map' : link.TEMPLATE}
  		    						to={link.TEMPLATE == "redirect" ? link.PATH+'/local-map' : link.PATH}
  		    						 > 
  		    							{link.LINKTITLE}  
  		    					</Link>
  		    				</h2> 
  		    				{link.subpages ? this.renderSubPageContainer(link) : null }
  		    			</div>
  		    		) 
            }
		    	})}
	    	</div>
	    )
	}
  }


  // renders the sublink elements of a parent nav link
  renderSubPageContainer(link) {

        // var subOpen = this.state.activeSubLink == "sublink-container-"+link.SLUG ? this.state.subLinksOpen : "closing" 
        var subOpen = this.state.activeSubLink == "sublink-container-"+link.TEMPLATE ? this.state.subLinksOpen : "closing" 

      
  	return (
	  	 <div 
	  	
	  	 className={"sublink-container " + subOpen} 
	  	 // id={"sublink-container-" + link.SLUG}
       id={"sublinke-container-" + link.TEMPLATE}
       key={link.ID}>
	  	 	{link.subpages.map(sub => {
	  	 		return <h2 key={sub.ID} className="sublink"><SubLink   to={sub.PATH} title={sub.LINKTITLE} >  </SubLink></h2> 
	  	 	})}
		</div>
		)
  }

 
  // these are elements that have sublinks
  handleSubLinks(link) {
  	if( link.SLUG == 'your-event' || link.SLUG == 'your-wedding' || link.SLUG == 'map'){
	    var subpages = ContentStore.getChildrenByID(link.ID)
	     var subpages_arr = new Array();
          _.each(subpages, function(p) {
              subpages_arr.push(p);
          });
	    	link.subpages = subpages_arr

	}
	return link
  }

  // on click opens the sublinks of a parent link
  handleMouseOver = (event) => {

    var subClass;
    if(event.target.id){
      if(event.target.className == "link-title"){

        subClass = event.target.id

      }
      if(document.getElementById(event.target.id).children[1]){
        subClass = document.getElementById(event.target.id).children[1].id
      } else {
       subClass = document.getElementById(event.target.id).parentNode.nextSibling.id
      }

   

     PageActions.setWhichSubLink(subClass)

    	PageActions.setSubLinksOpen("opening");
      var  el =  event ? document.getElementById(event.target.id) : document;
   
    	el.addEventListener('transistionend', this.listenerOpen)
    }
  }

  listenerOpen = (event) => {
  	PageActions.setSubLinksOpen("open");
  	event.target.removeEventListener(event.type, this.listenerOpen)
  }


  // clicking on another nav element should close subnav elements
  handleMouseOut = (event) => {


	  	PageActions.setSubLinksOpen("closing");

	  	var  el =  event ? document.getElementById(event.target.id) : document;
	  	el.addEventListener('transistionend', this.listenerClose)

  }

  listenerClose(event){
  	PageActions.setSubLinksOpen("close");
  	event.target.removeEventListener(event.type, this.listenerClose)
  }

  openLegal() {
  $('.legal').addClass('display-legal')

  }

  closeLegal() {
    $('.legal').removeClass('display-legal')
  }


}




class SubLink extends React.Component {
	constructor(props) { 
	
  	super(props);

  }


  render(){
  	return (
  		<Link  to={this.props.to} activeClassName="active-link">{this.props.title}</Link>
  	)
  }
}

SubLink.contextTypes = {
	router: React.PropTypes.object
}







