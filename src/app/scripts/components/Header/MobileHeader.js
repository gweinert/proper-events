import PageStore from '../../stores/PageStore.js';
import PageActions from '../../actions/PageActions.js';
import { Link } from 'react-router'


export default class MobileHeader extends React.Component {
  static propTypes = { 
    myNav: React.PropTypes.array 
  }
  
  constructor(props) { 
	
  	super(props);

  }

  state = {
  	navOpen: PageStore.getNavOpen
  };

  componentDidMount(){
  	PageStore.listen(this.onChange)
  }

  onChange = (state) => {

  	this.setState({navOpen: state.navOpen})

  };

  handleMobileNavClick = (e) => {
	    if(this.state.navOpen){
	      PageActions.setNavOpen(false)
	    }else{
	      PageActions.setNavOpen(true)
	    }
  };

  componentWillUnmount(){
  	PageStore.unlisten(this.onChange)
  }


  render(){

	var navOpen = this.state.navOpen ? "open" : "close"
  	
  	return (
  		<div className="mobile-header" id="mobile-nav">
          <div className={"mobile-header-img " + navOpen }>
            <Link to={this.props.subSitePath}><img src="/dist/release/proper-logo.png" /></Link>
            <div className={"open-hamburger " + navOpen } onClick={this.handleMobileNavClick} />
          </div>
        </div>
  	)
  
  }

}