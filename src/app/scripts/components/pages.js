/*----------------------------------------------------------------------------*\
 TITLE: COMPONENTS.JS
 DESCRIPTION: THIS FILE RETURNS AN OBJECT THAT MAPS PAGE COMPONENTS TO THEIR
              TEMPLATE NAMES. USED IN MAIN.JS TO ASSOCIATE PAGE COMPONENTS WITH
              ROUTES ACCORDING TO THE ROUTE'S TEMPLATE NAME.
 AUTHOR: MARK HOLMES
 NOTES: IF YOU NEED TO ADD ADDITIONAL PAGE COMPONENTS, ADD IT WITHIN THE OBJECT
        BELOW. MAKE SURE YOU CHECK THE ADMIN AREA WITHIN THE CMS TO DISCOVER THE
        CORRECT TEMPLATE NAME.
\*----------------------------------------------------------------------------*/

import Default  from './Default/Default.js';
import Home     from './Home/Home.js';
import Venues   from './Views/Venues.js';
import About    from './Views/About.js';
import Event    from './Views/Event.js';
import Proposal from './Views/Proposal.js';
import Food     from './Views/Food.js';
import Audio    from './Views/Audio.js';
import Gallery  from './Views/Gallery.js';
import Map      from './Views/Map.js';
import Terms      from './Views/Terms.js';
import Story      from './Views/Story.js';
import Partners   from './Views/Partners.js';
import Legal      from './Views/Legal.js';
import PropMap    from './Views/PropMap.js';
import SimpleMap      from './SimpleMap.js';


export default {
  'default': Default,
  'meetings_home': Home,
  'weddings_home': Home,
  'about': About,
  'gallery': Gallery,
  'lander_left': Event,
  'proposal': Proposal,
  'food': Food,
  'audio': Audio,
  'venue': Venues,
  'map': SimpleMap,
  'property_map': PropMap,
  'terms': Terms,
  'our_story': Story,
  'your-wedding': Proposal,
  'partners': Partners,
  'legal': Legal
};
