

import CSSTransitionGroup from 'react-addons-css-transition-group';
import PageStore from '../stores/PageStore.js';
var Swipeable = require('react-swipeable')


class Slideshow extends React.Component {

	constructor(props) {
		super(props)
	}

	state = {
		currentSlide : 0,
		canSlide: true,
		direction: "left"

	}

	static defaultProps = {
		autoPlay: false,
		controls: false,
		className: '',
		slideInfo: []
	}

	static propTypes = {
		autoPlay: React.PropTypes.bool
	}


	componentDidMount() {

		this.startAutoPlay()

		var el = document.getElementsByClassName(this.props.className)[0]

		document.addEventListener('keydown',  this.handleKeyPress )
		
	}

	componentWillUnmount() {
		clearInterval(this.state.myInterval)
		document.removeEventListener('keydown',  this.handleKeyPress )
		
	}

	handleKeyPress = (e) => {
		if( e.keyCode === 39){
			e.preventDefault()
			if(this.state.canSlide){
				this.handleNextClick()
				this.setState({clickArrow: "right"})
			}
		} else if ( e.keyCode == 37){
			if(this.state.canSlide){
				this.handlePrevClick()
				this.setState({clickArrow: "left"})
			}
		}
	}

	startAutoPlay() {
		
		if(this.props.autoPlay){
		
			var myInterval = window.setInterval( () => {
				this.handleNextClick()
			}, 5000);
			this.setState({myInterval: myInterval})
		}
		else{
	
			clearInterval(this.state.myInterval)
		}
	}

	componentDidUpdate(prevProps, prevState){

		if(prevProps.autoPlay != this.props.autoPlay){
			// this.startAutoPlay()
		}
		
	}

	handlePrevClick = () => { 

		// this.setState({direction: "right"})
		this.setState({direction: "left"})

		var current = this.state.currentSlide;
		var prev = current - 1;
		if(prev < 0 ) {
			prev = this.props.slides.length - 1
		}

		if(this.state.canSlide){
			this.setState({currentSlide: prev})
			this.setState({canSlide: false})
		}
		
		window.setTimeout( () => {
			this.setState({canSlide: true})
		}, 700)


		//	resets slideshow interval
		if(this.props.autoPlay){
			clearInterval(this.state.myInterval)
			var myInterval = window.setInterval( () => {
					this.handleNextClick()
				}, 5000);
				this.setState({myInterval: myInterval})
			}
		}

	handleNextClick = () => {
	
		// this.setState({direction: "left"})
		this.setState({direction: "right"})
		
		var current = this.state.currentSlide;
		var next = current + 1;
		if (next > this.props.slides.length - 1) {
			next = 0;
		}
		if(this.state.canSlide){
			this.setState({currentSlide : next})
			this.setState({canSlide: false})
		}
		window.setTimeout( () => {
			this.setState({canSlide: true})
		}, 700)

		if(this.props.autoPlay){
			clearInterval(this.state.myInterval)
			var myInterval = window.setInterval( () => {
					this.handleNextClick()
				}, 5000);
				this.setState({myInterval: myInterval})
			}
		}

	render() {

		//dupes array
		var slides = this.props.slides.slice();

		return (
			
			<div className={"slideshow " + this.props.className} ref="slideshow" >
				<Swipeable className="slideshow-swipe" onSwipedLeft={this.handleNextClick} onSwipedRight={this.handlePrevClick}>
				<Slides 
					className={this.props.className}
					slides={slides}
					slideInfo = {this.props.slideInfo}
					currentSlide={this.state.currentSlide} 
					direction={this.state.direction}/>
	
				{this.props.controls ? <Controls onPrevClick={this.handlePrevClick} onNextClick={this.handleNextClick}/> : null }
				</Swipeable>
			</div>
			
		)
	}

}

export default Slideshow;

// 	Slides

class Slides extends React.Component {
	
	constructor(props) {
		super(props)
	}

	state = {
		index: 0,
		reverse: false
	}

	componentWillReceiveProps(nextProps){

		if(this.props.className === "gallery"){
			if( nextProps.direction == "left"  ){
				this.setState({reverse: true})
			} else if( nextProps.direction == "right"){
				this.setState({reverse: false})
			}
		} else{
		
			if( nextProps.currentSlide == this.props.slides.length - 1 && this.props.currentSlide == 0) {
				this.setState({reverse: true})
			}
			else {

				if( 
					( nextProps.currentSlide > this.props.currentSlide )  || 
					( nextProps.currentSlide == 0 && this.props.currentSlide == this.props.slides.length - 1 ) 
				){
					this.setState({reverse: false})
				} 
				else {
					this.setState({reverse: true})
				}
			}
		}
		
	}


	shouldComponentUpdate(nextProps) {


		if (this.props.currentSlide != nextProps.currentSlide){

			return true
		} 

		
		else {
			return false;
		}
	}

	
	render() {


		var style = {
			backgroundImage: "url(" + CONTENT_DIR + "slides/" + this.props.slides[this.props.currentSlide].IMAGE1 + ")"

		}

		// var slideTransition = this.state.reverse ? "slideshow-slide-reverse" : "slideshow-slide"
		var slideTransition = this.props.direction == "left" ? "slideshow-slide-reverse" : "slideshow-slide"
		
		return (
			<div className="slides">
				
		        <CSSTransitionGroup transitionName={ slideTransition } transitionEnterTimeout={700} transitionLeaveTimeout={700}>
		        	{this.renderSlideInfo() }
		        	<div style={style} key={this.props.currentSlide} className="slide" ></div>
		        </CSSTransitionGroup>
				
			</div>
		)
	}

	renderSlideInfo() {
		var currentSlideInfo = this.props.slideInfo[this.props.currentSlide];

		if(currentSlideInfo){
			var infoNodes = Object.keys(currentSlideInfo).map( key => {
				if( (key.substring(0,5) == "BLURB" ) || key == "H1"){
			
					return (
						<SlideInfoNode
							type={key} 
							currentSlide={this.props.currentSlide}
							slideText={currentSlideInfo[key]} />
					)
				}
			})

			return (
				<div className={'slide-info-container'} id={"slide-info-"+this.props.currentSlide} key={'slide-info-'+this.props.currentSlide}>
					{infoNodes}
				</div>
			)
		}
		else {
			return null
		}
	}

}

class SlideInfoNode extends React.Component {
	constructor(props) {
		super(props)
	}

	state = {
		open: false,
		animating: false,
		canClick: true
	}

	componentDidMount() {
		var myEl = React.findDOMNode(this.refs.myNode)
		// myEl.addEventListener(transitionend, handler)
	}

	hide = () =>{

		if(this.state.canClick){

			this.setState({animating: "closing"});
			this.setState({canClick: false})
			setTimeout( this.handleTimeout, 1000)
		}
		
	}

	show = () => {


		if(this.state.canClick ){
			this.setState({animating: "opening"});
			this.setState({canClick: false})
			setTimeout( this.handleTimeout, 1000)
		}
	}

	handleTimeout = () => {
		this.setState({animating: false, open: !this.state.open})
		this.setState({canClick: true})
	}
	
	render() {

		var textState = this.state.open ? "open" : "close"
		var animating = this.state.animating 
		var animating2 = this.state.animating + "2"
		// var nodeTextType = this.props.type == "H1" ? this.props.type = "title" : this.props.type.toLowerCase()

		// <CSSTransitionGroup transitionName={"info-text-wipe-"+textState} transitionEnterTimeout={10000} transitionLeaveTimeout={10000}>
		if(this.props.slideText){
			return (
				
				<div 
					className={"slide-info-blurb "+this.props.type.toLowerCase() + " " + textState + " " + animating} 
					key={textState} 
					ref="myNode"
					id={"slide-"+this.props.currentSlide +"-"+this.props.type.toLowerCase() }>


			    		<div 
			    			onClick={ this.state.open ? this.hide : this.show } 
			    			className={"close-button " + animating + " " + textState} >
			    			<div className={"shadow-circle " + animating + " " + textState}></div>
			    			<div className={"left-x close-x " + animating+ " " + textState }>
				    			<div className={"right-x close-x " + animating + " " + textState}></div>
			    			</div>
			    		</div>

			    		
				    		<div className={"text-container "+ animating + " " + textState + " " + animating2} key={textState}>
			    				<p className={ "slide-node-pre "+animating + " " + textState}>{this.props.slideText}</p>
			    			</div>
		    			
				</div>
				


			)
		} else {
			return(null)
		}
	}
}

// class Slide extends React.Component {
// 	constructor(props) {
// 		super(props)
// 	}

// 	render() {

// 		return(
// 			<div className={this.props.active ? "slide--active slide" : "slide"}>
// 				<img src={this.props.src} />
// 			</div>
// 		)
// 	}
// }


//Prev and next buttons

class Controls extends React.Component {
	constructor(props){
		super(props)
	}

	state = PageStore.getState();

	componentDidMount() {

		 PageStore.listen(this.onChange);
	}

	componentWillUnmount() {
		PageStore.unlisten(this.onChange)
	}

	componentDidUpdate() {
		if(this.state.navOpen) {
			this.refs.next.style.right = '290px'
		}
		else {
			this.refs.next.style.right = '40px'
		}
	}

	onChange = (state) => {

		this.setState(state)
		
	}


	togglePrev = () => {
		this.props.onPrevClick();
	}

	toggleNext =() => {
		this.props.onNextClick();
	}

	render() {

		var showControls = this.props.controls ? "show-controls" : "hide-controls"

		return (
			<div className={"controls " + showControls}>
				<div className="toggle toggle--prev" onClick={this.togglePrev} ></div>
				<div className="toggle toggle-next" onClick={this.toggleNext} ref="next"></div>
			</div>
		)
	}


}


















