require('malihu-custom-scrollbar-plugin')($);
require('jquery-mousewheel');
 require('react-dom')
 import PageStore from '../stores/PageStore.js';
import PageActions from '../actions/PageActions.js';

export default class Scroll extends React.Component {

	constructor(props) {
		super(props)
	}

	static defaultProps = {
		scrollInertia: 150,
		destroyOnMobile: true,
		callbacks: {
			onInit: function(){},
			onScrollStart: function(){},
			onScroll: function(){},
			whileScrolling: function(){},
			onTotalScroll: function(){},
	        onTotalScrollBack: function(){},
	        onOverflowY: function(){},
	        onOverflowX: function(){},
	        onOverflowYNone: function(){},
	        onOverflowXNone: function(){},
	        onUpdate: function(){},
	        onImageLoad: function(){},
	        onSelectorChange: function(){}
		}
	}

	state = {
		scrollExists: false,
		animating: false
	};

	componentWillMount(){
		// if( $('.mCustomScrollbar').length > 1 ){
		// 	this.setState({scrollExists: true})
		// 	console.log('dsfh')
		// }
	}

	componentDidMount(){
		var that = this

		PageStore.listen(this.onChange);

		var resizeTimer


		var buildToggle = function(){

				if( window.innerWidth > 820 || !that.props.destroyOnMobile) {
	
						that.buildScroll()

				}
				else {
					that.destroyScroll()
				}
		}

		if( window.innerWidth > 820 || !that.props.destroyOnMobile) {
			window.setTimeout(function(){
				buildToggle(that.props.destroyOnMobile)

			}.bind(this), 70)
		}

		// buildToggle(true)

		// $(window).resize(function(){
		// 	clearTimeout(resizeTimer)
		// 	resizeTimer = setTimeout(function(){
		// 		buildToggle(that.props.destroyOnMobile)
		// 	}.bind(this), 100)
		// })

		document.addEventListener( 'resize', function(){

			clearTimeout(resizeTimer)
			resizeTimer = setTimeout(function(){
				buildToggle(that.props.destroyOnMobile)
			}.bind(this), 100)
		})
	}

	onChange = (state) => {

    this.setState({animating: state.animating})
	this.setState({scrollExists: state.scrollExists})
  
  }

	componentWillUnmount(){
	    PageStore.unlisten(this.onChange)
		// document.removeEventListener( 'resize', function() {
		// 	clearTimeout(resizeTimer)
		// 	resizeTimer = setTimeout(function(){
		// 		buildToggle(that.props.destroyOnMobile)
		// 	}.bind(this), 100)
		// })
	}

	


	buildScroll = () => {
		// $(this.getDOMNode()).mCustomScrollbar(this.props)
		// console.log("build scrolll")
		$(ReactDOM.findDOMNode(this)).mCustomScrollbar(this.props)
		PageActions.setScrollExists(true)
		
	};



	destroyScroll = () => {
		// $(this.getDOMNode()).mCustomScrollbar('destroy')
		// console.log("destroy scroll")
		$(ReactDOM.findDOMNode(this)).mCustomScrollbar('destroy')
	};



	render(){
		return(
			<div className={this.props.className} >
				{this.props.children}
			</div>
		)
	}


}













