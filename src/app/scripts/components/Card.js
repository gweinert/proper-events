import PageStore from '../stores/PageStore.js';
import CSSTransitionGroup from 'react-addons-css-transition-group';
 require('react-dom')

import Scroll from './Scroll.js';


export default class Card extends React.Component {
  
	constructor(props) { 

  		super(props);

 	}

  state = {
          navOpen: PageStore.getNavOpen,
          buildScroll: false,
        }



  componentDidMount = (e) => {

    PageStore.listen(this.onChange);

    window.addEventListener('resize', this.handleResize);
    
    window.setTimeout( () => {
      this.handleResize()
    }, 300)

    this.needScroll()

    
    ReactDOM.findDOMNode(this).style.webkitTransition = "500ms all ease"



  
  }

  shouldComponentUpdate(prevState, prevProps){

    if(prevState.navOpen != this.state.navOpen){
      return true
    }
    else{
      return false
    }
  }



  componentDidUpdate(prevState){

    //  if card has changed state - make sure content is centered
 
      this.handleContainerPosition()


  }



  componentWillUnmount = (e) => {
    PageStore.unlisten(this.onChange)
    window.removeEventListener('resize', this.handleResize)
  }



  handleResize = () => {
    this.handleContainerPosition()

    this.needScroll()

  }


  // if content is larger than container, tells page to build a scrollbar
  needScroll = () => {
    var textContainer = document.getElementsByClassName('text-container')[0]
    var card = document.getElementsByClassName('card')[0]

    if(window.innerWidth > 820){

      if(textContainer.offsetHeight > card.offsetHeight){
        this.setState({buildScroll: true})

      }
      else{
        this.setState({buildScroll: false})
      }

    }
    else{
      this.setState({buildScroll: false})
    }

  };


  //  handles different container centering styles depending on if there is a scroll bar or not
  handleContainerPosition(){
    var textContainer = document.getElementsByClassName('text-container')[0]
    var card = document.getElementsByClassName('card')[0]
    var height = textContainer.offsetHeight

    if(window.innerWidth > 820){
    
      if( card.offsetHeight > height ){
        textContainer.style.marginTop = '-'+Math.min(height/2 + .5)+'px'
        textContainer.style.top = '50%'
        textContainer.style.position = 'absolute'
        $('.mCustomScrollbar').css('position', 'static')
        
      }
      else {
        textContainer.style.marginTop = '0px'
        textContainer.style.top = 'auto'
        textContainer.style.position = 'static'
      }
    }
  }

  onChange = (state) => {
    if(this.state.navOpen != state.navOpen){
      this.setState({navOpen: state.navOpen})
    }
  
  }


	render() {


      // var slideDirection = this.state.navOpen ? "left" : "right" 
      var state = this.state.navOpen ?  ' open' : ' close'

      var style
      if(this.state.navOpen){
        style={ right: '279px'}
      }else{
        style={ right: '20px'}
      }
     
      var cardHtml = (
        
          <div 
              className={  this.props.className + ' ' + state + ' ' } 
              style={style}
              >

              { this.state.buildScroll ? <Scroll>{this.props.blurbs}</Scroll> : <div className="no-scroll">{this.props.blurbs}</div> }
              

            </div>
       
      )

      if( window.innerWidth < 820 ){
        return cardHtml
      }
      else {

        return (

             cardHtml
          
      	);
      }
  	}
}

// module.exports = Card;
