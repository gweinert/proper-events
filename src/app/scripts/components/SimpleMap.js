import GoogleMap                  from 'google-map-react';
import shouldPureComponentUpdate  from 'react-pure-render/function';
import PageActions                from '../actions/PageActions.js';
import MapIcon                    from '../../images/proper-map-circle.png';
import GoogMapIcon                from '../../images/black-marker.png';

export default class SimpleMap extends React.Component {
  static defaultProps = {
    center: {lat: 59.938043, lng: 30.337157},
    zoom: 15,
    greatPlaceCoords: {lat: 59.724465, lng: 30.080121}
  };
 
  shouldComponentUpdate = shouldPureComponentUpdate;
  
  constructor(props) {
    super(props);
  }

  state = {
    currentMarker: null,
    lat: null,
    lng: null
  };

  componentDidMount() {
     if(window.innerWidth < 820){
      $('.content-area').addClass('mobile-full');
    }
    PageActions.setSubLinksOpen("opening")
    PageActions.setWhichSubLink('sublink-container-redirect')

  }

  componentWillMount() {
    $('html').addClass('no-overflow')
    window.setTimeout( () =>{
      $('html').removeClass('no-overflow')
    }, 1000)
  }

  componentWillUnmount() {
    $('.slideshow.default').show();
    if(window.innerWidth < 820){
      $('.content-area').removeClass('mobile-full');
    }
  }

  createMapOptions(maps) {
    return {
      styles: require('./../../styles/mapstyles.json')
    }
  };

  handleClick = (e, lat, lng) => {

    if(e.target.className == "close"){

      this.handleClose()
    }
    else{
      this.setState({ currentMarker: e.currentTarget.id })
      this.setState({lat: lat})
      this.setState({lng: lng})
    }
  };

  handleClose = () => {

    this.setState({currentMarker: null})
  };



  renderMapMarkers = () => {
    console.log("render map makrers", this)
    var markers = this.props.contentArea.CONTENT_SLIDES.FIELDGROUP1

    if(markers){

      return (
        markers.map( (marker) => {
          return(
            <Marker
              lat={marker.LATITUDE}
              lng={marker.LONGITUDE}
              defaultZoom={this.props.zoom}
              marker={marker}
              id={marker.ID}
              key={marker.ID}
              currentMarker={this.state.currentMarker}
              icon={GoogMapIcon}
              clickHandler={this.handleClick}
              closeHandler={this.handleClose} />
            )
        })
      )
    }
    else{
      return null
    }

    

  }

 
  render() {

    var myStyle ={ position: 'abosolute'}
    var content = this.props.contentArea

    var lat = this.state.lat != null ? this.state.lat : this.props.contentArea.LATITUDE
    var lng = this.state.lng != null ? this.state.lng : this.props.contentArea.LONGITUDE
    var center = {lat: lat, lng: lng}

    console.log("map props", content)
    var renderMapMarkers = Object.keys(this.props.contentArea.CONTENT_SLIDES).length > 0

    var mar = false
    

    return (
       <GoogleMap style={myStyle}
        defaultCenter={ {lat: content.LATITUDE, lng: content.LONGITUDE} }
        defaultZoom={this.props.zoom}
        center={center}
        options={this.createMapOptions}

        >

          <Marker
              lat={content.LATITUDE}
              lng={content.LONGITUDE}
              defaultZoom={this.props.zoom}
              marker={content}
              id={content.ID}
              key={content.ID}
              currentMarker={this.state.currentMarker}
              icon={MapIcon}
              clickHandler={this.handleClick}
              closeHandler={this.handleClose} 
          />

          { renderMapMarkers ? this.renderMapMarkers() : <div lat={content.LATITUDE} lng={content.LONGITUDE}></div> }
                            
          
        
      </GoogleMap>
    );
  }
}

class Marker extends React.Component {
  // static propTypes = {
  //   text: PropTypes.string
  // };

  state = {
    showContent: false,
    displayContent: false,
    keepContentOpen: false,
    contentPosition: null
  };

  shouldComponentUpdate = shouldPureComponentUpdate;

  constructor(props) {
    super(props);
  }

  componentWillMount() {
     
     PageActions.setKeepSubNavOpen(true);

  
  }

  
  componentDidMount() {

    PageActions.setMainNavActiveLink('map')
    $('.slideshow.default').hide();

    
  }

  
  componentDidUpdate(nextProps){

     if( $('html').hasClass('isMobile') ) {

       if(this.props.currentMarker != this.props.id){
        this.setState({keepContentOpen: false})
        this.closeContent()
       }
     }


     
  }

 
  handleMouseOver = (e) => {

    if( !$('html').hasClass('isMobile') ){
      
      this.openContent()

    }
  
  };

  
  handleMouseLeave = (e) => {

    if( !$('html').hasClass('isMobile') ){

      this.closeContent()

    }
  
  };

  
  handleClick = (e) => {
    
    if( $('html').hasClass('isMobile') ){
      this.openContent()
       this.setState({keepContentOpen: true})
       this.props.clickHandler(e, this.props.lat, this.props.lng)
        // this.openContent()
       // this.setState({keepContentOpen: true})
    }
  
  }

  
   //  sets it to display block and then change opacity
  openContent = () => {

    if(!this.state.displayContent){
     
      this.setState({displayContent: true})

      window.setTimeout( () => {
        this.setState({showContent: true})

        this.setState({contentHeight: this.refs.markerBox.offsetHeight + 30})
        this.setState({contentPosition: this.refs.markerBox.offsetHeight + 20 })
        
      }, 100)
    }
  
  };



  //  sets it to change opacity and then display none
  closeContent = () => {

    if(!this.state.keepContentOpen){
      console.log("close content")
        this.setState({showContent: false})
        window.setTimeout( () => {
          this.setState({displayContent: false})
        }, 250)
    }

  };



  render() {

    const K_WIDTH = 40;
    const K_HEIGHT = 40;

    const greatPlaceStyle = {
      // initially any map object has left top corner at lat lng coordinates
      // it's on you to set object origin to 0,0 coordinates
      position: 'absolute',
      width: K_WIDTH,
      height: K_HEIGHT,
      left: -K_WIDTH / 2,
      top: -K_HEIGHT / 2,
      // backgroundImage: 'url(' + CONTENT_DIR + 'slides/' + this.props.icon +')'
      backgroundImage: 'url(' + this.props.icon + ')'
    };

    var contentStyle = {
      bottom: this.state.contentPosition  + 'px',
      height: this.state.contentHeight
    }

    var displayState = this.state.displayContent ? "block" : "none"
    var contentState = this.state.showContent ? "show" : "hide"
    var renderClose = $('html').hasClass('isMobile')

    var addressLink = this.props.marker.BUTTONLINK2.length > 2 ? this.props.marker.BUTTONLINK2 : "http://maps.google.com/?ll="+this.props.lat+","+this.props.lng


    
    return (
     
          <div 
            id={this.props.id}
            className="marker" 
            style={greatPlaceStyle}
            key={this.props.id}
            
            onMouseOver={this.handleMouseOver}
            onMouseLeave={this.handleMouseLeave}
            onClick={this.handleClick}
            
            
          >
              <div ref="markerContent" 
                style={contentStyle} 
                className={"marker-content " + contentState + " " + displayState}
                >
                <div className="marker-box" ref="markerBox">
                  
                  
                  
                  <p className="marker-title" >{this.props.marker.H1}</p>

                  { renderClose ? 
                    <div className="close" 
                      onClick={this.props.closeClickHandler}>
                      x
                    </div> : null }
                  
                  {this.props.marker.ADDRESS1 ? <p className="marker-description" ><a href={addressLink} target="_blank">{this.props.marker.ADDRESS1}</a></p> : null }
                  
                  {this.props.marker.BUTTONLINK1 ? <p className="marker-link"><a href={this.props.marker.BUTTONLINK1} target='_blank'>{this.props.marker.BUTTONTEXT1}</a></p> : null }
                  
                  {this.props.marker.PHONE ? <p className="marker-phone"><a href={"tel:+1"+this.props.marker.PHONE}>{this.props.marker.PHONE}</a></p> : null }
                
                </div>
              </div>
            

        </div>
    );
  }
}








