export default class SiteLoader extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loaded: false
    };
  }

  componentDidMount(){
    var loader = $(React.findDOMNode(this)),
    scope = this;

    function setLoaded() {
      scope.setState({'loaded':true});
      $('body').addClass('loaded');
    }

    var catchAll = setTimeout(function(){
      setLoaded();
    }, 2000);

    $(window).on('site-loaded', function(){
      clearTimeout(catchAll);
      setTimeout(function(){
        setLoaded();
      }, 500);
    });
  }

  render() {

    return (
      <div id="site-loader" className={this.state.loaded ? 'loaded' : false}>
        <i className="spinner" style={{backgroundImage: 'url('+require('../../../img/cutlery.png')+')'}}/>
      </div>
    );
  }
};