import Page from './../Page/Page.js';

class Default extends React.Component {
  constructor(props) { super(props); }
  render() {
    return <div className={ this.props.className } ></div>;
  }
}

export default Page(Default);
