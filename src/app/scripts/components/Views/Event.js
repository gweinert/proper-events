import Card from '../Card.js';
import Page from './../Page/Page.js';
import ContentStore from './../../stores/ContentStore';
import PageActions from '../../actions/PageActions.js';

class Event extends React.Component {
  
	constructor(props) { 

  		super(props);
  		console.log("Event props", this)
      // state = PageStore.get
 	}

 	static defaultProps = {
 		blurbs: ["Event link", "Second Blurb on card"] 
 	}

  componentWillMount() {
     PageActions.setKeepSubNavOpen(true);
  }

  componentDidMount() {
    // PageStore.listen(this.onChange);
    PageActions.setSubLinksOpen("opening")
    
     if(this.props.subSitePath.indexOf('meeting') > 1){
        PageActions.setWhichSubLink('sublink-container-your-event')
       PageActions.setMainNavActiveLink('meetings_home')
       $('#meetings_home').addClass('active-link')
      }
      else if( this.props.subSitePath.indexOf('wedding') > 1){
        PageActions.setWhichSubLink('sublink-container-your-wedding')
        PageActions.setMainNavActiveLink('weddings_home')
      }
      else {
        PageActions.setWhichSubLink('sublink-container-your-event')
        PageActions.setMainNavActiveLink('venue')
      }
  }	

  onChange = (state) => {
    // console.log("state", this.state)
    // this.setState(state);
  }


	render() {
// console.log("render home?")
    var page = this.props.contentArea;
    var slides = page.CONTENT_SLIDES.FIELDGROUP1;

    	return (

    		<div className={ this.props.className }>
           
      		<Card className="card" blurbs={this.props.blurbs} id={page.ID}/>
  
	    	</div>
    	)
  	}
}

export default Page(Event);