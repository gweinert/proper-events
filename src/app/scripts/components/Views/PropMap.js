import Card from '../Card.js';
import Page from './../Page/Page.js';
import ContentStore from './../../stores/ContentStore';
import PageActions from '../../actions/PageActions.js';

class PropMap extends React.Component {
  
	constructor(props) { 

  		super(props);
  		console.log("PropMap props", this.props)
      // state = PageStore.get
 	}

 	static defaultProps = {
 		blurbs: ["PropMap link", "Second Blurb on card"] 
 	}

  componentWillMount() {
     PageActions.setKeepSubNavOpen(true);
  }

  componentDidMount() {
    // PageStore.listen(this.onChange);
    PageActions.setMainNavActiveLink('map')
    $("#map").addClass('active-link')
    $('.slideshow.default').hide();
    console.log(this)

    PageActions.setSubLinksOpen("opening")
    PageActions.setWhichSubLink('sublink-container-redirect')
  }	

  componentWillUnmount() {
    $('.slideshow.default').show();
  }
    

  onChange = (state) => {
    // console.log("state", this.state)
    // this.setState(state);
  }

  renderHtml() {
    var bgImg = { backgroundImage: 'url(' + CONTENT_DIR + '/slides/' + this.props.contentArea.IMAGE1 + ')'}
    return(
      <div className="map-container">
          <div className="map-img" style={bgImg}></div>
      </div>
    )
  }


	render() {
// console.log("render home?")
    var page = this.props.contentArea;
    var slides = page.CONTENT_SLIDES.FIELDGROUP1;
    var html = this.renderHtml()

    	return (

    		<div className={ this.props.className }>
           
      	 {html}
  
	    	</div>
    	)
  	}
}

export default Page(PropMap);