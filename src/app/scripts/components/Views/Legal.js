import Card from '../Card.js';
import Page from './../Page/Page.js';
import ContentStore from './../../stores/ContentStore';
import PageActions from '../../actions/PageActions.js';
import PageStore from '../../stores/PageStore.js';
import Scroll from    '../Scroll.js'



class Legal extends React.Component {
  
  constructor(props) { 

      super(props);
      // console.log("Legal props", this)
      // state = PageStore.get
  }

  static defaultProps = {
    blurbs: ["Proper living link", "Second Blurb on card"] 
  }

  state = {
    legalContent: ""
  }

  componentWillMount() {
 
  }

  componentDidMount() {

    var legalContent = ContentStore.getByPath(this.props.subSitePath + '/legal').BLURB1
    this.setState({legalContent: legalContent})
  }

  componentWillUnmount(){

  }

  onChange = (state) => {

  }

  hide = () => {
    $('.legal').removeClass('display-legal')
  }

  renderHtml() {


    return (
      <div className="text-container">

      <div className="close-button-container">
            <div onClick={ this.hide } className="close-button">
              <div className="left-x">
                <div className="right-x"></div>
              </div>
            </div> 
          </div>
        
        <div className="card-header">
          <h1>LEGAL/PRIVACY</h1>
        </div>
        <div className="intro-text">

          <Scroll>
        
            <p>{this.state.legalContent}</p>

          </Scroll>
        

        </div>
        

      

      
      

      </div>

    )
  }

  


  render() {


      return (

        <div className="legal">
           
          {this.renderHtml()}
  
        </div>
      )
    }
}

export default Page(Legal);