
import Card from '../Card.js';
import Page from './../Page/Page.js';
import ContentStore from './../../stores/ContentStore';
import PageActions from '../../actions/PageActions.js';
import PageStore from '../../stores/PageStore.js';
import Scroll from '../Scroll.js';

class Proposal extends React.Component {
  
	constructor(props) { 

  		super(props);

 	}

 	static defaultProps = {
 		blurbs: ["Proper living link", "Second Blurb on card"] 
 	}

  state = PageStore.getState()

  componentWillMount() {
    PageActions.setKeepSubNavOpen(true);

  }

  componentDidMount() {
    PageStore.listen(this.onChange);

    PageActions.setSubLinksOpen("opening")


     if(this.props.subSitePath.indexOf('meeting') > 1){
      PageActions.setWhichSubLink('sublink-container-meetings_home')
       PageActions.setMainNavActiveLink('meetings_home')
       $('#meetings_home').addClass('active-link')
      }
      else if( this.props.subSitePath.indexOf('wedding') > 1){
        PageActions.setWhichSubLink('sublink-container-weddings_home')
        PageActions.setMainNavActiveLink('weddings_home')
      }
      else {
        PageActions.setWhichSubLink('sublink-container-meetings_home')
        PageActions.setMainNavActiveLink('venue')
      }

  }

  componentWillUnmount(){
    PageStore.unlisten(this.onChange)
    // $('#meetings_home').removeClass('active-link')
  }

  onChange = (state) => {
    this.setState(state);
  }

  renderTableRows(object){
    return (
      <table>
        <tbody>
          {Object.keys(object).map(function(item) {
                
            return(
              <tr key={object[item].ID}>
                <td>{object[item].TITLE}</td>
                <td>{object[item].BLURB1}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    )
  }

  createMarkup(html){
      return{
        __html: html
      }
  }


  renderHtml = () => {
    
    var menuCategories = {}
    var tables;
    if(this.props.contentArea.CONTENT_SLIDES.MENU){
      this.props.contentArea.CONTENT_SLIDES.MENU.map( function(item) {
        if(!item.FIELDGROUPID){
          menuCategories[item.ID] = item
          menuCategories[item.ID].slides = []
        }
      })
      this.props.contentArea.CONTENT_SLIDES.MENU.map( function(item, index) {
        if(item.FIELDGROUPID){


          menuCategories[item.FIELDGROUPID].slides ? menuCategories[item.FIELDGROUPID].slides[index] = (item) : menuCategories[item.FIELDGROUPID].slides = [item]
        }
      })
    }

    tables = Object.keys(menuCategories).map( (key) => {

      return (
        <div key={key} className="date-table">
          
          <div className="table-header">
            <h2>{menuCategories[key].TITLE}</h2>
          </div>

            
            { this.renderTableRows(menuCategories[key].slides.sort( function(a, b) {

              return a.ID - b.ID
            })) }
          
        </div>
      )  
    })

    

    return (
     
        
        <div className="text-container">
          
          <div className="card-header">
            <h3>{this.props.contentArea.H1}</h3>
          </div>

          {tables}

          <div className="card-footer-text">
            <div dangerouslySetInnerHTML={this.createMarkup(this.props.contentArea.BLURB1)}></div>
          </div>

          {this.props.contentArea.BUTTONLINK1 ? this.renderButtons() : null }
        
        </div>

      

    )
  }

  renderButtons(){

    if(this.props.contentArea.BUTTONLINK2){
      var button2 = (
        <div className="button">
          <h2><a href={this.props.contentArea.BUTTONLINK2} target="_blank" >{this.props.contentArea.BUTTONTEXT2}</a></h2>
        </div>
        )
    }

    return (
      <div className="button-container">
        <div className="button">
<h2><a href={CONTENT_DIR+"pdfs/"+this.props.contentArea.BUTTONLINK1} target="_blank" >{this.props.contentArea.BUTTONTEXT1}</a></h2>        </div>
        {button2}
        
      </div>
    )
  }


	render() {
    var page = this.props.contentArea;
    var slides = page.CONTENT_SLIDES.FIELDGROUP1;
    var html = this.renderHtml();

    	return (

    		<div className={ this.props.className }>
           
      		<Card className="card" blurbs={html} id={page.ID}/>
  
	    	</div>
    	)
  	}
}

export default Page(Proposal);