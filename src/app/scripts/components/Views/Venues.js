import { Link } from 'react-router';
import Page from './../Page/Page.js';
import ContentStore from './../../stores/ContentStore';
import Slideshow from '../Slideshow.js';
import PageActions from '../../actions/PageActions.js';
import PageStore from '../../stores/PageStore.js';

export default class Venues extends React.Component {
	
	constructor(props) {
		super(props)
	}

	componentWillMount(){
		PageActions.setKeepSubNavOpen(false)
	}

	 componentDidMount() {
	 	this.props.slideInfo[0].title = this.props.contentArea.CONTENT_SLIDES.FIELDGROUP1[0].H1
	 	 PageActions.setKeepSubNavOpen(true);

	 	 PageActions.setSubLinksOpen("opening")

	 	 if(this.props.subSitePath.indexOf('meeting') > 1){
	 	 	PageActions.setWhichSubLink('sublink-container-meetings_home')
		 	 PageActions.setMainNavActiveLink('meetings_home')
		 	}
		 	else if( this.props.subSitePath.indexOf('wedding') > 1){
		 		PageActions.setWhichSubLink('sublink-container-weddings_home')
		 		PageActions.setMainNavActiveLink('weddings_home')
		 	}
		 	else {
		 		PageActions.setWhichSubLink('sublink-container-your-event')
		 		PageActions.setMainNavActiveLink('venue')
		 	}
	    // PageStore.listen(this.onChange);
	    // PageActions.setMainNavActiveLink('/gallery')
	    console.log("venues", this.props.contentArea)

	    if(window.innerWidth < 820 ){
	      this.setState({isMobile : true})
	      console.log("add mobile class")
	      $('.slideshow.gallery').addClass('mobile-full')
	    }
	  }	

	  onChange = (state) => {
	    // console.log("state", this.state)
	    // this.setState(state);
	  }

	  componentWillUnmount() {
	  	// PageStore.unlisten(this.onChange)
		  	if(window.innerWidth < 820 ){
	      this.setState({isMobile : true})
	      $('.slideshow.gallery').removeClass('mobile-full')
	    }
	  }

	static defaultProps = {
		slideInfo: [
			{
				name: 'Slide1',
				title: 'Penth',
				description: 'Lorem ipsum stuff and then there\nwere more words written out by me',
				blurb1: 'This is another blurb written by me',
				blurb2: 'Here is yet another blurb yay',
				blurb3: 'And this is blurb 3 wooop woop'
			}
		]
	}

	render(){
		// console.log("gallery", this.props)
		return(

			<div className={this.props.className} >
				<Slideshow 
					slides={this.props.contentArea.CONTENT_SLIDES.FIELDGROUP1} 
					slideInfo={this.props.contentArea.CONTENT_SLIDES.FIELDGROUP1} 
					controls = {true}
					className="gallery" />
			</div>

		)
	}
}