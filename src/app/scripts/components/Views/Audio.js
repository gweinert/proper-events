import Card from '../Card.js';
import Page from './../Page/Page.js';
import ContentStore from './../../stores/ContentStore';
import PageActions from '../../actions/PageActions.js';
import PageStore from '../../stores/PageStore.js';

class Audio extends React.Component {
  
	constructor(props) { 

  		super(props);
  		// console.log("Audio props", this)
      // state = PageStore.get
 	}

 	static defaultProps = {
 		blurbs: ["Proper living link", "Second Blurb on card"] 
 	}

  state = PageStore.getState()

  componentWillMount() {
    PageActions.setKeepSubNavOpen(true);
    PageActions.setMainNavActiveLink('/residences')
  }

  componentDidMount() {
    PageStore.listen(this.onChange);
    PageActions.setMainNavActiveLink('meetings_home')
    PageActions.setWhichSubLink('sublink-container-meetings_home')
    PageActions.setSubLinksOpen("opening")
    // PageActions.setKeepSubNavOpen(true);
    // console.log("did this work?", this.props)
  }

  componentWillUnmount(){
    PageStore.unlisten(this.onChange)
  }

  onChange = (state) => {
    // console.log("Audio state", this.state)
    this.setState(state);
  }

  createMarkup(html){
    return{
      __html: html
    }
  }

  renderHtml() {

    var page = this.props.contentArea;

    return (
      <div className="text-container">
        
        <div className="card-header">
          <h1>{page.H1}</h1>
        </div>
        <div className="intro-text">
          <div dangerouslySetInnerHTML={this.createMarkup(page.BLURB1)}></div>
        </div>
        <div className="about-header">
          <h1>{page.H2}</h1>
        </div>
        <div className="about-text">
          <div dangerouslySetInnerHTML={this.createMarkup(page.BLURB2)}></div>
        </div>

      

        {page.BUTTONTEXT1 ? this.renderButtons() : null }
      

      </div>

    )
  }

  renderButtons(){

    if(this.props.contentArea.BUTTONLINK2){
      var button2 = (
        <div className="button">
          <h2><a href={this.props.contentArea.BUTTONLINK2} target="_blank" >{this.props.contentArea.BUTTONTEXT2}</a></h2>
        </div>
        )
    }

    return (
      <div className="button-container">
        <div className="button">
<h2><a href={CONTENT_DIR+"pdfs/"+this.props.contentArea.BUTTONLINK1} target="_blank" >{this.props.contentArea.BUTTONTEXT1}</a></h2>        </div>
        {button2}
        
      </div>
    )
  }


	render() {
// console.log("render home?")
    var page = this.props.contentArea;
    var slides = page.CONTENT_SLIDES.FIELDGROUP1;
    var html = this.renderHtml();

    	return (

    		<div className={ this.props.className }>
           
      		<Card className="card" blurbs={html} id={page.ID}/>
  
	    	</div>
    	)
  	}
}

export default Page(Audio);