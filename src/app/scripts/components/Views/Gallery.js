import Card from '../Card.js';
import Page from './../Page/Page.js';
import ContentStore from './../../stores/ContentStore';
import PageActions from '../../actions/PageActions.js';

class Gallery extends React.Component {
  
	constructor(props) { 

  		super(props);
  		console.log("Gallery props", this)
      // state = PageStore.get
 	}

 	static defaultProps = {
 		blurbs: ["Proper living link", "Second Blurb on card"] 
 	}

  state = {
    isMobile : false
  }

  componentWillMount(){
    PageActions.setKeepSubNavOpen(false)
  }

  componentDidMount() {
    // PageStore.listen(this.onChange);
    PageActions.setSlideShowAutoPlay(true)
    PageActions.setMainNavActiveLink('gallery')

    if(window.innerWidth < 820 ){
      this.setState({isMobile : true})
      console.log("add mobile class")
      $('.slideshow.default').addClass('mobile-full')
    }
  }	

  componentWillUnmount(){
    if(window.innerWidth < 820 ){
      this.setState({isMobile : true})
      $('.slideshow.default').removeClass('mobile-full')
    }
  }

  onChange = (state) => {
    // console.log("state", this.state)
    // this.setState(state);
  }


	render() {
// console.log("render home?")
    var page = this.props.contentArea;
    var slides = page.CONTENT_SLIDES.FIELDGROUP;
    var mobileGallery = this.state.isMobile ? "full-screen " : ""

    	return (

    		<div className={ this.props.className + " " + mobileGallery}>
           
      		
  
	    	</div>
    	)
  	}
}

export default Page(Gallery);