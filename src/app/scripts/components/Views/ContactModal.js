import React from 'react';
import PageStore from '../../stores/PageStore.js';
import PageActions from '../../actions/PageActions.js';

export default class ContactModal extends React.Component {

	state = {
		showModal: PageStore.getModalShow(),
		formSuccess: false
	}

	componentDidMount() {
		PageStore.listen(this.onChange);
		console.log("modal mounted ", this.props)
	}

	onChange = (state) => {
		if(state.showModal != this.state.showModal){
			this.setState({showModal: state.showModal})

		}
	}

	handleCloseClick = () => {
		this.setState({formSuccess: false})
		PageActions.setModalShow(false)
	}

	clearErrors(){
		var commentField = document.getElementById('contactComment')
		commentField.className = ""
	}

	handleSubmit = () => {
		
		if( this.validForm() ){	
			this.sendForm()
		}
		
	}

	validForm(){
		var commentField = document.getElementById('contactComment')
		var commentValue = commentField.value
		
		if(commentValue.length > 1){
			return true;
		} else{
			commentField.className = " error"
			return false;
		}
	}


	sendForm(){
		var comments = document.getElementById('contactComment').value
		
		var formObj = {
			comments: comments,
			path: this.props.subSitePath
		}

		$.ajax({
	        type: "POST",
	        url: "/api/v1/Contact_us",
	        contentType: "application/json; charset=utf-8",
	        accepts: {
	        	xml: 'text/xml',
	        	text: 'text/plain'
	        },
	        dataType: "json",
	        data: JSON.stringify(formObj)

	    }).done( function( data ){
	    	console.log("done data", data)
	    	this.formSuccess()
	    }.bind(this))
	    .fail(function( data ){
			console.log("fail data", data)
	    })
	    .complete( function(data){
	    	console.log("complete data")
	    	
	    }.bind(this))
	}

	formSuccess(){
		document.getElementById('contactComment').value = ""
		this.setState({formSuccess: true})
	}


	renderAddressLink = () => {
		var address1 = this.props.content.ADDRESS1
		var address2 = this.props.content.ADDRESS2
		var mapQuery = encodeURIComponent(address1) + '' + encodeURIComponent(address2)
		var mapLink = "https://maps.google.com/?q=" + mapQuery

		return(
			<div className="address-container">
				<a href={mapLink} target='_blank'>
					<div className="address" >
						<p>{ address1 }</p>
						<p>{ address2 }</p>
					</div>
				</a>
			</div>
			
		)
	}

	renderPhoneLink = () => {
		var phone = this.props.content.H2
		var phoneNum = phone.replace(/\D/g, '')

		return(
			<div className="phone-container">
				<a href={"tel:" + phoneNum}><p>{phone}</p></a> 
			</div>
		)
	}

	render() {
		var showFormSuccess = this.state.formSuccess ? 'hide' : 'show'
		var showModal = this.state.showModal ? 'show' : 'hide'
		return (
			<div className={"modal-overlay " + showModal}>
				<div className={"modal " + showModal}>
					<div className="close" onClick={this.handleCloseClick}></div>
					<div className="modal-header">
						<h1>Contact {this.props.content.H1}</h1>
					</div>

					<div className="contact-container">
						
						{this.renderAddressLink()}

						{this.renderPhoneLink()}
						<div className="email-container">
							<a href={"mailto:"+ this.props.content.H3}><p className="email">{this.props.content.H3}</p></a>
						</div>

					</div>

					<div className="form-container">
						<form id="myForm" className={"form "+ showFormSuccess}> 

							<div className="form-control">
								<label for="comments">Comments</label>
								<textarea onClick={this.clearErrors}name="comments" id="contactComment"></textarea>
								
							</div>

							<div className="submit-button" 
							onClick={this.handleSubmit}
							>
								<p>SUBMIT</p>
							</div>

						</form>

						<div className={"form-success " + showFormSuccess}>
							<h1> Thank You! </h1>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
