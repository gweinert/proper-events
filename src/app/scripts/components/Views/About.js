
import Card from '../Card.js';
import Page from './../Page/Page.js';
import ContentStore from './../../stores/ContentStore';
import PageActions from '../../actions/PageActions.js';

class About extends React.Component {
  
	constructor(props) { 

  		super(props);
  		console.log("ABOUT props", this)
      // state = PageStore.get
 	}

 	static defaultProps = {
 		blurbs: ["Proper living link", "Second Blurb on card"] 
 	}

  componentWillMount(){
    PageActions.setKeepSubNavOpen(false)
    PageActions.setNavOpen(true)
  }

  componentDidMount() {
    // PageStore.listen(this.onChange);
    PageActions.setMainNavActiveLink('about')
  }	

  onChange = (state) => {
    // console.log("state", this.state)
    // this.setState(state);
  }

  createMarkup(html){
    return{
      __html: html
    }
  }

  renderHtml(){

    return (
      <div>
        <div className="text-container">
          <div className="card-header">
                <h1>{this.props.contentArea.H1}</h1>
          </div>
          <div className="intro-text">
            <div dangerouslySetInnerHTML={this.createMarkup(this.props.contentArea.BLURB1)}></div>
          </div>

          {this.props.contentArea.BUTTONLINK1 ? this.renderButtons() : null }
          
        </div>

      </div>
    )
  }

  renderButtons(){

    if(this.props.contentArea.BUTTONLINK2){
      var button2 = (
        <div className="button">
          <h2><a href={this.props.contentArea.BUTTONLINK2} target="_blank" >{this.props.contentArea.BUTTONTEXT2}</a></h2>
        </div>
        )
    }

    return (
      <div className="button-container">
        <div className="button">
        <h2><a href={CONTENT_DIR+"pdfs/"+this.props.contentArea.BUTTONLINK1} target="_blank" >{this.props.contentArea.BUTTONTEXT1}</a></h2>        </div>
        {button2}
        
      </div>
    )
  }


	render() {
// console.log("render home?")
    var page = this.props.contentArea;
    console.log("page", page)
    var slides = page.CONTENT_SLIDES.FIELDGROUP1;
    var html = this.renderHtml()

    	return (

    		<div className={ this.props.className }>
           
      		<Card className="card" blurbs={html} id={this.props.contentArea.ID}/>
  
	    	</div>
    	)
  	}
}

export default Page(About);
