
import Card from '../Card.js';
import Page from './../Page/Page.js';
import ContentStore from './../../stores/ContentStore';
import PageActions from '../../actions/PageActions.js';
import PageStore from '../../stores/PageStore.js';

class Food extends React.Component {
  
  constructor(props) { 

      super(props);

  }

  static defaultProps = {
    blurbs: ["Proper living link", "Second Blurb on card"] 
  }

  state = PageStore.getState()

  componentWillMount() {
    PageActions.setKeepSubNavOpen(true);

  }

  componentDidMount() {
    PageStore.listen(this.onChange);
    
    PageActions.setSubLinksOpen("opening")
    
     if(this.props.subSitePath.indexOf('meeting') > 1){
        PageActions.setWhichSubLink('sublink-container-meetings_home')
       PageActions.setMainNavActiveLink('meetings_home')
       $('#meetings_home').addClass('active-link')
      }
      else if( this.props.subSitePath.indexOf('wedding') > 1){
        PageActions.setWhichSubLink('sublink-container-weddings_home')
        PageActions.setMainNavActiveLink('weddings_home')
      }
      else {
        PageActions.setWhichSubLink('sublink-container-your-event')
        PageActions.setMainNavActiveLink('venue')
      }

  }

  componentWillUnmount(){
    PageStore.unlisten(this.onChange)
  }

  onChange = (state) => {
    this.setState(state);
  }

  renderTableRows(object){
    return (
      <table className="table-no-flick">
        {Object.keys(object).map(function(item, index) {
              
          return(
            <tr className="table-no-flick" key={index}>
              <td>{object[item].TITLE}</td>
              <td>{object[item].BLURB1}</td>
            </tr>
          )
        })}
      </table>
    )
  }

   createMarkup(html){
      return{
        __html: html
      }
  }


  renderHtml = () => {
    
    var menuCategories = {}
    var tables;

    if(this.props.contentArea.CONTENT_SLIDES.MENU){
      this.props.contentArea.CONTENT_SLIDES.MENU.map( function(item) {
        if(!item.FIELDGROUPID){
          menuCategories[item.ID] = item
          menuCategories[item.ID].slides = []
        }
      })
      this.props.contentArea.CONTENT_SLIDES.MENU.map( function(item, index) {
        if(item.FIELDGROUPID){

 
          menuCategories[item.FIELDGROUPID].slides ? menuCategories[item.FIELDGROUPID].slides[index] = (item) : menuCategories[item.FIELDGROUPID].slides = [item]
        }
      })

      tables = Object.keys(menuCategories).map( (key) => {
 
        return (
          <div className="date-table">
            
            <div className="table-header">
              <h2>{menuCategories[key].TITLE}</h2>
            </div>

              
              { this.renderTableRows(menuCategories[key].slides.sort( function(a, b) {
        
                return a.ID - b.ID
              })) }
            
          </div>
        )  
      })
    }

    
    // console.log("menu cat", menuCategories);
    return (
      <div className="text-container">
        
        <div className="card-header">
          <h3>{this.props.contentArea.H1}</h3>
        </div>

        {tables ? tables : null}

        <div className="card-footer-text">
          <div dangerouslySetInnerHTML={this.createMarkup(this.props.contentArea.BLURB1)}></div>
        </div>

         {this.props.contentArea.BUTTONLINK1 ? this.renderButtons() : null }
      

      </div>

    )
  }

  renderButtons(){

    if(this.props.contentArea.BUTTONLINK2){
      var button2 = (
        <div className="button">
          <h2><a href={this.props.contentArea.BUTTONLINK2} target="_blank" >{this.props.contentArea.BUTTONTEXT2}</a></h2>
        </div>
        )
    }

    return (
      <div className="button-container">
        <div className="button">
<h2><a href={CONTENT_DIR+"pdfs/"+this.props.contentArea.BUTTONLINK1} target="_blank" >{this.props.contentArea.BUTTONTEXT1}</a></h2>        </div>
        {button2}
        
      </div>
    )
  }


  render() {
    var page = this.props.contentArea;
    var slides = page.CONTENT_SLIDES.FIELDGROUP1;
    var html = this.renderHtml();

      return (

        <div className={ this.props.className }>
           
          <Card className="card" blurbs={html} id={page.ID}/>
  
        </div>
      )
    }
}

export default Page(Food);