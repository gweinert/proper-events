import Page from './../Page/Page.js';
import Card from '../Card.js';
import PageStore from '../../stores/PageStore.js';
import PageActions from '../../actions/PageActions.js';



class Home extends React.Component {
  
	constructor(props) { 

  		super(props);
  		// console.log("HOME props", this)
      // state = PageStore.get
 	}

 	static defaultProps = {
 		blurbs: ["First blurb on card", "Second Blurb on card"],
    test: "GARRETT"
 	}

  componentDidMount() {
    var el = this.refs.text



      PageActions.setKeepSubNavOpen(true);

      PageActions.setSubLinksOpen("opening")
      
       if(this.props.subSitePath.indexOf('meeting') > 1){
          PageActions.setWhichSubLink('sublink-container-meetings_home')
         PageActions.setMainNavActiveLink('meetings_home')
         $('#meetings_home').addClass('active-link')
        }
        else if( this.props.subSitePath.indexOf('wedding') > 1){
          PageActions.setWhichSubLink('sublink-container-weddings_home')
          PageActions.setMainNavActiveLink('weddings_home')
        }
        else {
          PageActions.setWhichSubLink('sublink-container-your-event')
          PageActions.setMainNavActiveLink('venue')
        }

    
    
  }	

  onChange = (state) => {
    // console.log("state", this.state)
    // this.setState(state);
  }

  handleTest() {

  }

  renderButtons(){

    if(this.props.contentArea.BUTTONLINK2){
      var button2 = (
        <div className="button">
          <h2><a href={this.props.contentArea.BUTTONLINK2} target="_blank" >{this.props.contentArea.BUTTONTEXT2}</a></h2>
        </div>
        )
    }

    return (
      <div className="button-container">
        <div className="button">
          <h2><a href={CONTENT_DIR+"pdfs/"+this.props.contentArea.BUTTONLINK1} target="_blank" >{this.props.contentArea.BUTTONTEXT1}</a></h2>
        </div>
        {button2}
        
      </div>
    )
  }

  createMarkup(html){
    return{
      __html: html
    }
  }

	render() {

   


    var page = this.props.contentArea;
    var slides = page.CONTENT_SLIDES.FIELDGROUP1;
    var html =( 
      <div>
        <div className="text-container" ref={(c) => { this.myText = c } } >
          <div className="card-header">
                <h1>{ page.H1 }</h1>
          </div>
          <div className="intro-text">
            <div dangerouslySetInnerHTML={this.createMarkup(page.BLURB1)}></div>
            
          </div>
          <div className="about-header">
            <h1 onClick={this.handleTest}>{ page.H2 }</h1>
          </div>
          <div className="about-text">
            <div dangerouslySetInnerHTML={this.createMarkup(page.BLURB2)}></div>
          </div>


          {page.BUTTONTEXT1 ? this.renderButtons() : null }
          
        </div>
        
      </div>)

    	return (

    		<div className={ this.props.className }>


    			
           
      		<Card className="card" blurbs={html} key={this.props.contentArea.ID}>

            
          </Card>
  
	    	</div>
    	)
  	}
}

export default Page(Home);
