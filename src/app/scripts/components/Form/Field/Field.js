export default class Field extends React.Component {
  static defaultProps = {
    errorMessage: undefined,
    label: undefined,
    placeholder: undefined,
    validate: false
  }
  static propTypes = {
    errorMessage: React.PropTypes.oneOfType([
      React.PropTypes.bool,
      React.PropTypes.string
    ]),
    id: React.PropTypes.string.isRequired,
    label: React.PropTypes.oneOfType([
      React.PropTypes.bool,
      React.PropTypes.string
    ]),
    placeholder: React.PropTypes.oneOfType([
      React.PropTypes.bool,
      React.PropTypes.string
    ]),
    type: React.PropTypes.string.isRequired,
    value: React.PropTypes.oneOfType([
      React.PropTypes.bool,
      React.PropTypes.number,
      React.PropTypes.string
    ]),
    validate: React.PropTypes.bool.isRequired
  }
  constructor(props) {
    super(props);
    this.state = {
      error: this.props.validate ? false : undefined,
      value: this.props.value,
      validate: this.props.validate ? ::this.validate : undefined
    }
  }
  componentDidMount() { this.getState(); }
  formatClassName() {
    return [
      `form__field`,
      `form__field--${ this.props.type }`,
      `${ this.props.id }`,
      `${ this.state.error ? `form__error` : '' }`
    ].join(' ');
  }
  formatLabel() {
    const label = this.props.id.replace(/[\W\_]/g, ' ');
    return label.replace(/\b\w/g, (character) => {
      return character.toUpperCase();
    });
  }
  formatPlaceholder() {
    return this.props.placeholder ? this.formatLabel() : undefined;
  }
  getState() {
    let value;
    let field = this.refs[this.props.type];
    switch (this.props.type) {
      case 'checkbox': value = field.checked; break;
      default: value = field.value; break;
    }
    this.props.handleOnChange(this.props.id, value);
    const error = this.props.validate ? false : undefined;
    this.setState({ value, error });
  }
  handleOnChange() { this.getState() }
  validate(){
    let error;
    switch (this.props.type) {
      case 'email': error = this.validateEmail(); break;
      case 'text':
      case 'textarea': error = this.validateText(); break;
      default: error = false;
    }
    this.setState({ error });
    return error;
  }
  validateEmail() {
    const regEx = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    switch(this.state.value) {
      case null:
      case undefined:
      case '': return true;
      case this.state.value:
        return !regEx.test(this.state.value);
      default: return false;
    }
    return this.state.value === '';
  }
  validateText() {
    switch (this.state.value) {
      case null:
      case undefined:
      case '': return true;
      default: return false;
    }
  }
  render() {
    const props = {
      id: this.props.id,
      onChange: ::this.handleOnChange,
      placeholder: this.formatPlaceholder(),
      ref: this.props.type,
      type: this.props.type,
      value: this.props.value
    };
    return props.type !== 'hidden' ? (
      <div className={ this.formatClassName() }>
        { this.props.label ? (
          <label data-value={ this.state.value } htmlFor={ props.id }>
            { this.props.label === true ? (
              this.formatLabel()
            ) : ( this.props.label ) }
          </label>
        ) : ( null )}
        {(() => {
          switch (props.type) {
            case 'select':
              return (
                <select { ...props }>
                  { this.props.children }
                </select>
              );
            case 'textarea':
              return <textarea { ...props } />;
            default:
              return <input { ...props } />;
          }
        })()}
        { this.state.error && this.props.errorMessage ? (
          <div className="form__error__message">
            { `${ this.props.errorMessage } ${ this.formatLabel() }`}
          </div>
        ) : ( null ) }
      </div>
    ) : ( <input { ...props } /> );
  }
}
