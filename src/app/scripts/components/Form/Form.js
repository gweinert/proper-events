import API from './../../sources/API.js';
import Field from './Field/Field.js';

class Form extends React.Component {
  static defaultProps = {
    className: '',
    errorMessages: 'Please Provide Valid',
    id: 'form',
    labels: true,
    placeholders: false,
    noValidate: true,
    submitButtonText: 'Submit',
    successMessage: "Thank You!"
  }
  static propTypes = {
    action: React.PropTypes.string.isRequired,
    className: React.PropTypes.string.isRequired,
    errorMessages: React.PropTypes.oneOfType([
      React.PropTypes.bool,
      React.PropTypes.string
    ]).isRequired,
    id: React.PropTypes.string.isRequired,
    labels: React.PropTypes.bool.isRequired,
    placeholders: React.PropTypes.bool.isRequired,
    noValidate: React.PropTypes.bool.isRequired,
    submitButtonText: React.PropTypes.string.isRequired,
    successMessage: React.PropTypes.string.isRequired
  }
  constructor(props) {
    super(props);
    this.state = {
      form: {},
      success: false,
      validated: false
    };
  }
  componentDidMount() { this.buildState(this.props.children); }
  buildChildren(children) {
    children = children.length ? children : [children];
    return children.map((field, i) => {
      return (
        React.cloneElement(field, {
          errorMessage: this.formatErrorMessage(field.props),
          handleOnChange: ::this.handleOnChange,
          key: i,
          label: this.formatLabel(field.props.label),
          placeholder: this.formatPlaceholder(field.props.placeholder),
          ref: field.props.id,
          value: field.props.checked || field.props.value || undefined
        })
      );
    });
  }
  buildState(children) {
    const form = {};
    children = children.length ? children : [children];
    children.map((child, i) => {
      let value = '';
      const field = this.refs[child.props.id].refs[child.props.type];
      switch (child.props.type) {
        case 'checkbox': value = field.checked; break;
        default: value = field.value; break;
      }
      form[child.props.id] = value;
    });
    this.setState({ form });
  }
  formatErrorMessage(props) {
    if (props.validate) {
      const message = props.errorMessage;
      return message !== undefined ? message : this.props.errorMessages;
    } else {
      return undefined;
    }
  }
  formatLabel(label) {
    return label !== undefined ? label : this.props.labels;
  }
  formatPlaceholder(placeholder) {
    return placeholder !== undefined ? placeholder : this.props.placeholders;
  }
  handleOnChange(id, state) {
    const form = this.state.form;
    form[id] = state;
    this.setState({ form });
  }
  handleOnSubmit(e) {
    e.preventDefault();
    this.validate(() => {
      API.post(this.props.action, this.state.form).then((response) => {
        this.setState({ success: !this.state.success });
      }).catch((error) => {
        console.log(error);
      });
    });
  }
  validate(cb) {
    return Object.keys(this.refs).filter((ref, i) => {
      const fieldState = this.refs[ref].state;
      const validate = this.refs[ref].props.validate;
      return validate ? fieldState.validate() : false;
    }).length === 0 ? this.setState({ validated: true}, () => {
      cb();
    }) : this.setState({ validated: false });
  }
  render() {
    const props = {
      id: this.props.id,
      noValidate: this.props.noValidate,
      onSubmit: ::this.handleOnSubmit
    };
    return (
      <div className={`${this.props.className} form`}>
        { this.state.success ? (
          <div className="form__success">
            <div className="form__success__message">
              { this.props.successMessage }
            </div>
          </div>
        ) : (
          <form { ...props }>
            { this.buildChildren(this.props.children) }
            <div className="form__field form__field--submit">
              <button
                className="form__field--submit__button"
                form={ this.props.id }
                type="submit">
                { this.props.submitButtonText }
              </button>
            </div>
          </form>
        )}
      </div>
    );
  }
}

export { Form, Field };
