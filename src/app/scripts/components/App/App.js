/*----------------------------------------------------------------------------*\
 TITLE: APP.JSX
 DESCRIPTION: THIS FILE BUILDS THE MAIN CONTAINER COMPONENT FOR THE APP.
 AUTHOR: MARK HOLMES
 NOTES:
\*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*\
 DEPENDENCIES
\*----------------------------------------------------------------------------*/

import ContentStore from './../../stores/ContentStore.js';
import PageStore from '../../stores/PageStore.js';
import CSSTransitionGroup from 'react-addons-css-transition-group';
import Footer from './../Footer/Footer.js';
import Header from './../Header/Header.js';
import MobileHeader from './../Header/MobileHeader';
import Legal from './../Views/Legal.js';
import ContactModal from './../Views/ContactModal.js';
// import Slideshow from '../Slideshow.js';
import Slideshow from '../Slideshow.js'
import { name } from './../../../../../package.json';
import PageActions from '../../actions/PageActions.js';
import { Link } from 'react-router'

/*----------------------------------------------------------------------------*\
 CLASS DEFINITION
\*----------------------------------------------------------------------------*/

export default class App extends React.Component {
  static defaultProps = {
    history: {},
    location: {},
    params: {}
  }
  static propTypes = {
    history: React.PropTypes.object,
    location: React.PropTypes.object,
    params: React.PropTypes.object
  }
  constructor(props) {
    super(props);
    this.state = {
      className: 'app',
      loading: true,
      projectName: this.titleize(name),
      navOpen: false,
      transition: {
        transitionAppear: true,
        transitionAppearTimeout: 500,
        className: 'content-area',
        component: 'div',
        transitionEnterTimeout: 500,
        transitionLeaveTimeout: 500,
        transitionName: 'page-transition'
      }
    };
  }
  componentDidMount() {
    // console.log("APP mounted")
    let pathname = this.props.location.pathname;
    this.setPageTitle(pathname);
    this.setState({ loading: !this.state.loading });
    this.detectMobile()
  }
  componentWillReceiveProps(nextProps) {
    let pathname = this.props.location.pathname;
    this.setPageTitle(pathname);
  }

  detectMobile(){
    if( navigator.userAgent.toLowerCase().match(/android/i)
   || navigator.userAgent.toLowerCase().match(/webos/i)
   || navigator.userAgent.toLowerCase().match(/iphone/i)
   || navigator.userAgent.toLowerCase().match(/ipad/i)
   || navigator.userAgent.toLowerCase().match(/ipod/i)
   || navigator.userAgent.toLowerCase().match(/blackberry/i)
   || navigator.userAgent.toLowerCase().match(/windows phone/i)
   ){
    // return true;
    $('html').addClass('isMobile')

    }
   else {
      // return false;
    }
  }

  formatPageClass(pathname) {
    let className = pathname.replace(/\//g, '-').slice(1);
    return className === '' ? 'home' : className;
  }
  setPageTitle(pathname) {
    pathname = this.titleize(pathname);
    pathname = pathname === '' ? 'Home' : pathname;
    document.title = `${ this.state.projectName } | ${ pathname }`;
  }
  titleize(str) {
    return str
      .replace(/\W/g, ' ')
      .trim()
      .replace(/\b\w/g, (character) => {
        return character.toUpperCase();
      });
  }


  render() {
    const pathname = this.props.location.pathname;


    // finds path to determine whether this is a meeting-event, wedding, or residence site.
    var subSitePathLength = 0;
    for( var i = 0 ; i < pathname.length ; i++ ){
      if( pathname[i] === "/" ){
        subSitePathLength += 1
      }
      // if(subSitePathLength === 3) {
      //   subSitePathLength = pathname.length
      // }
      if(subSitePathLength === 4) {
        subSitePathLength = i;
        break;

      }
    }

    if(subSitePathLength === 3) {
        subSitePathLength = pathname.length
      }
    // if(subSitePathLength === 4) {
    //     subSitePathLength = i;

    //   }


    const subSitePath = this.props.location.pathname.substring(0, subSitePathLength)


    const galleryPath = subSitePath + "/gallery"
    const subSitePathID = ContentStore.getByPath(subSitePath).ID
    const { className, transition } = this.state;
    const contentArea = ContentStore.getByPath(pathname);
   var template = contentArea.TEMPLATE

   var aboutContent = ContentStore.getByPath(subSitePath + '')
    
    if( template == "redirect" ) {
      template = contentArea.TEMPLATE_CHILD

    }

    // if we are on gallery page, start autoplay
    var autoPlay = template == "gallery" ? true : false

    var legalStyle = {
      display: 'none'
    }

    // gets footer/header content of site depending on which site user is on
    if(subSitePath.indexOf('meeting') > 1){
        var socialContent = ContentStore.getByPath(subSitePath + '/your-event')
    }
    else if( subSitePath.indexOf('wedding') > 1){
          var socialContent = ContentStore.getByPath(subSitePath +'/your-wedding')
        }
    else {
      var socialContent = ContentStore.getByPath(subSitePath +'')
    }

    var contactContent = ContentStore.getByTemplate('contact')[0]



       
    return (
      <div className={ className } >
        

        <Header nav={ ContentStore.getChildrenByID(subSitePathID) } socialContent={socialContent} key={this.state.navOpen} activeLink={template} subSitePath={subSitePath} aboutContent={aboutContent} />
        <MobileHeader subSitePath={subSitePath}/>
       
         <Slideshow slides={ ContentStore.getByPath(galleryPath).CONTENT_SLIDES.FIELDGROUP1} className="default" autoPlay={false} controls={autoPlay}/>
        <CSSTransitionGroup { ...this.state.transition } >
          { React.cloneElement(this.props.children, {
            className: `page ${ this.formatPageClass(pathname) } ${ template }`,
            contentArea,
            key: pathname,
            subSitePath: subSitePath
          }) }
        </CSSTransitionGroup>

         <Legal className="legal" style={legalStyle} subSitePath={subSitePath}/>
         <ContactModal content={contactContent} subSitePath={subSitePath}/>
       
        <Footer nav={ ContentStore.getChildrenByPath('/footer') } />
      </div>
    );
  }
}
