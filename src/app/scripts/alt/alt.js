import Alt from 'alt';
const alt = new Alt();
if (ENV === 'dev' || ENV === 'local') {
  Alt.debug('alt', alt);
}
export default alt;
