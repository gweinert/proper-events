import Alt from './../alt/alt.js';


class PageActions {

	setAnimate(bool) {
		this.dispatch(bool);
  	}

    setScrollExists(bool) {
      this.dispatch(bool)
    }


  	setNavWidth(width) {
  		// console.log("actions width", width)
  		this.dispatch(width);
  	}

  	setNavOpen(bool) {
  		this.dispatch(bool);
  	}


    setSubLinksOpen(state){
      this.dispatch(state)
    }

    setKeepSubNavOpen(bool){
      // console.log('set sub nav', bool)
      this.dispatch(bool)
    }

    setMainNavActiveLink(link){
      this.dispatch(link)
    }

    setSlideShowAutoPlay(bool){
      this.dispatch(bool)
    }

    setWhichSubLink(link){
      this.dispatch(link)
    }

    setModalShow(bool){
      this.dispatch(bool)
    }

}

export default Alt.createActions(PageActions);
