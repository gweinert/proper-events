import Alt from './../alt/alt.js';
import API from './../sources/API.js';

class ContentActions {
  
  get(cb = () => {}) { this.actions.getContentAreas(cb); }
  
  
  getContentAreas(cb) {
    API.get('v2/content_areas/', {
      'where': 'active=1 AND deleted <> 1'
    }).then((response) => {
      const contentAreas = response.DATA;
      this.actions.getContentSlides(contentAreas, cb);
    }).catch((error) => {
      this.actions.getFail(error);
      cb(error);
    });
  }
  

  getContentSlides(contentAreas, cb) {
    API.get('v2/content_slides/', {
      'where': 'deleted <> 1'
    }).then((response) => {
      const contentSlides = response.DATA;
      const payload = { contentAreas, contentSlides };
      this.actions.getSuccess(payload);
      cb(payload);
    }).catch((error) => {
      this.actions.getFail(error);
      cb(error);
    });
  }
  

  getSuccess(payload) { this.dispatch(payload); }
  

  getFail(error) { this.dispatch(error); }


}

export default Alt.createActions(ContentActions);
