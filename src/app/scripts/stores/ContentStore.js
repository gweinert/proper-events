import Alt from './../alt/alt.js';
import ContentActions from './../actions/ContentActions.js';

class ContentStore {
  constructor() {
    this.contentAreas = {};
    this.bindListeners({
      onGetFail: ContentActions.GET_FAIL,
      onGetSuccess: ContentActions.GET_SUCCESS
    });
    this.exportPublicMethods({
      getAll: this.getAll,
      getByID: this.getByID,
      getByPath: this.getByPath,
      getByTemplate: this.getByTemplate,
      getChildrenByID: this.getChildrenByID,
      getChildrenByPath: this.getChildrenByPath,
      getParentByID: this.getParentByID,
      getParentByPath: this.getParentByPath
    });
  }
  addContentAreas(payload) {
    const contentAreas = {};
    payload.contentAreas.forEach((contentArea, i) => {
      contentArea.CONTENT_SLIDES = {};
      contentAreas[`${contentArea.ID}`] = contentArea;
    });
    this.addContentSlides(contentAreas, payload.contentSlides);
  }
  addContentSlides(contentAreas, contentSlides) {
    contentSlides.forEach((contentSlide, i) => {
      if (contentAreas[`${contentSlide.AREAID}`]) {
        const fieldGroup = contentSlide.FIELDGROUP.toUpperCase();
        const contentArea = contentAreas[`${contentSlide.AREAID}`];
        const contentSlides = contentArea.CONTENT_SLIDES[fieldGroup] || [];
        contentArea.CONTENT_SLIDES[fieldGroup] = [...contentSlides, contentSlide];
        contentArea.CONTENT_SLIDES[fieldGroup].sort((a, b) => {
          return a.SORTORDER - b.SORTORDER;
        });
      }
    });
    // console.log("this.contentAreas", this.contentAreas)
    // console.log("content Areas", contentAreas)
    contentAreas = Object.assign({}, this.contentAreas, contentAreas);
    this.setState({ contentAreas });
    // console.log('new this. ', this.contentAreas)

  }
  getAll() { 
    return this.getState().contentAreas; 
  }
  getByID(id) {
    const contentAreas = this.getState().contentAreas;
    return contentAreas[Object.keys(contentAreas).find((contentArea, i) => {
      return contentAreas[contentArea].ID === id;
    })] || [];
  }
  getByPath(path) {
    const contentAreas = this.getState().contentAreas;
    return contentAreas[Object.keys(contentAreas).find((contentArea, i) => {
       return contentAreas[contentArea].PATH === path;
    })] || [];
  }
  getByTemplate(template) {
    const contentAreas = this.getState().contentAreas;
    return Object.keys(contentAreas).filter((contentArea, i) => {
      return contentAreas[contentArea].TEMPLATE === template;
    }).map((contentArea, i) => {
      return contentAreas[contentArea];
    }).sort((a, b) => {
      return a.SORTORDER - b.SORTORDER;
    });
  }
  getChildrenByID(id) {
    const contentAreas = this.getState().contentAreas;
    return Object.keys(contentAreas).filter((contentArea, i) => {
      return contentAreas[contentArea].PARENTID === id;
    }).map((contentArea, i) => {
      return contentAreas[contentArea];
    }).sort((a, b) => {
      return a.SORTORDER - b.SORTORDER;
    });
  }
  getChildrenByPath(path) {
    const ID = this.getByPath(path).ID;
    return this.getChildrenByID(ID);
  }
  getParentByID(id) {
    const ID = this.getByID(id).PARENTID;
    return this.getByID(ID);
  }
  getParentByPath(path) {
    const ID = this.getByPath(path).PARENTID;
    return this.getByID(ID);
  }
  onGetFail(error) { console.log(error); }
  
  onGetSuccess(payload) { 
    this.addContentAreas(payload);

  }
}

export default Alt.createStore(ContentStore, 'ContentStore');
