import Alt from './../alt/alt.js';
import PageActions from '../actions/PageActions.js';

class PageStore {
  
  constructor() {
    this.animating = false;
    this.navWidth = 280;
    this.navOpen = true;
    this.keepSubNavOpen = false;
    this.subLinksOpen = "closed";
    this.activeLink = null;
    this.slideShowAutoPlay = false;
    this.activeSubLink = null;
    this.scrollExists = false;
    this.showModal = false;
     this.bindListeners({
      onSetAnimate: PageActions.SET_ANIMATE,
      onSetNavWidth: PageActions.SET_NAV_WIDTH,
      onSetNavOpen: PageActions.SET_NAV_OPEN,
      onSetSubLinksOpen: PageActions.SET_SUB_LINKS_OPEN,
      onSetKeepSubNavOpen: PageActions.SET_KEEP_SUB_NAV_OPEN,
      onSetMainNavActiveLink: PageActions.SET_MAIN_NAV_ACTIVE_LINK,
      onSetSlideShowAutoPlay: PageActions.SET_SLIDE_SHOW_AUTO_PLAY,
      onSetWhichSubLink: PageActions.SET_WHICH_SUB_LINK,
      onSetScrollExists: PageActions.SET_SCROLL_EXISTS,
      onSetModalShow: PageActions.SET_MODAL_SHOW
    });

    this.exportPublicMethods({
      getAutoPlay: this.getAutoPlay,
      getNavOpen: this.getNavOpen,
      getModalShow: this.getModalShow
    })
  }

  onSetAnimate(bool){

  	this.animating = bool;
  }


  onSetNavWidth(width){

  	this.navWidth = width;
  }


  onSetNavOpen(bool){
  	this.navOpen = bool;
  }

  onSetSubLinksOpen(state){
    this.subLinksOpen = state;
  }

  onSetKeepSubNavOpen(bool){
    this.keepSubNavOpen = bool
  }

  onSetMainNavActiveLink(link){
    this.activeLink = link
  }


  onSetSlideShowAutoPlay(bool){
    this.slideShowAutoPlay = bool
  }

  getAutoPlay(){
    return this.slideShowAutoPlay
  }

  getNavOpen(){
    return this.navOpen
  }

  onSetWhichSubLink(link){
    this.activeSubLink = link;
  }

  onSetScrollExists(bool){
    this.scrollExists = bool
  }

  onSetModalShow(bool){
    this.showModal = bool
  }

  getModalShow(){
    return this.showModal
  }

}

export default Alt.createStore(PageStore, 'PageStore');