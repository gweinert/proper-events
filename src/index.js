/*----------------------------------------------------------------------------*\
 TITLE: INDEX.JS
 DESCRIPTION: THIS FILE IS THE MAIN ENTRY POINT OF THE APP.
 AUTHOR: MARK HOLMES
 NOTES:
\*----------------------------------------------------------------------------*/

import './app/markup/index.jade';
import './app/styles/main.scss';
import 'babel-polyfill';
import './app/scripts/main.js';
