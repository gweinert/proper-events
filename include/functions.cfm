<cfscript>

function nocache(){
  try {
    var package = DeserializeJSON(FileRead(ExpandPath('/') & "package.json"));
    return package.version;
  } catch(any e){
    return "0";
  }
}

function QueryToStruct(query, row = 0){
  var loc = StructNew();
  if (ARGUMENTS.Row){
    loc.FromIndex = ARGUMENTS.Row;
    loc.ToIndex = ARGUMENTS.Row;
  } else {
    loc.FromIndex = 1;
    loc.ToIndex = ARGUMENTS.Query.RecordCount;
  }

  loc.Columns = ARGUMENTS.Query.getMetaData().getColumnLabels();
  loc.ColumnCount = ArrayLen(loc.Columns);
  loc.DataArray = ArrayNew(1);

  for (loc.RowIndex = loc.FromIndex ; loc.RowIndex LTE loc.ToIndex ; loc.RowIndex = (loc.RowIndex + 1)){
    ArrayAppend(loc.DataArray, StructNew());
    loc.DataArrayIndex = ArrayLen(loc.DataArray);
    for (loc.ColumnIndex = 1 ; loc.ColumnIndex LTE loc.ColumnCount ; loc.ColumnIndex = (loc.ColumnIndex + 1)){
      loc.ColumnName = loc.Columns[loc.ColumnIndex];
      loc.DataArray[loc.DataArrayIndex][loc.ColumnName] = ARGUMENTS.Query[loc.ColumnName][loc.RowIndex];
    }
  }

  if (ARGUMENTS.Row){
    return(loc.DataArray[1]);
  } else {
    return(loc.DataArray);
  }
};

</cfscript>
