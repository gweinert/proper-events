<cfquery name="GetNav">
  SELECT id, parentID, linkTitle, path
  FROM content_areas
  WHERE active = 1
  AND deleted <> 1
</cfquery>

<cfquery name="GetContentAreas">
  SELECT id, parentID, linkTitle, path
  FROM content_areas
  WHERE active = 1
  AND deleted <> 1
</cfquery>

<cfquery name="GetContentSlides">
  SELECT id, parentID, linkTitle, path
  FROM content_areas
  WHERE active = 1
  AND deleted <> 1
</cfquery>

<cfsavecontent variable="bootstrap">
  <cfoutput>
    <script>
      var STORES = #SerializeJSON({
        "nav" = QueryToStruct(GetNav),
        "content_areas" = QueryToStruct(GetContentAreas),
        "content_slides" = QueryToStruct(GetContentSlides)
      })#;
    </script>
  </cfoutput>
</cfsavecontent>
