<cfquery name="GetMeta">
  SELECT pageTitle, meta_keywords, meta_description
  FROM content_areas
  WHERE path = '#CGI.PATH_INFO#'
</cfquery>

<cfsavecontent variable="meta">
  <cfoutput>
    <title>#GetMeta.pageTitle#</title>
    <meta name="keywords" content="#GetMeta.meta_keywords#">
    <meta name="description" content="#GetMeta.meta_description#">
  </cfoutput>
</cfsavecontent>
