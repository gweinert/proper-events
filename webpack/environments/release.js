module.exports = (() => {

  "use strict";

  const loaders = require('./../config/release-loaders.js');
  const plugins = require('./../config/plugins.js');

  return {
    entry: { main: [ './src/index.js' ] },
    output: {
      filename: '[name].[hash].js',
      path: './dist/release/',
      publicPath: '/dist/release/'
    },
    module: { loaders: loaders },
    plugins: plugins
  };

})();
