module.exports = (() => {

  "use strict";

  const devServer = require('./../config/dev-server.js');
  const loaders = require('./../config/local-loaders.js');
  const plugins = require('./../config/plugins.js');

  return {
    devServer: devServer,
    entry: {
      main: [
        'webpack/hot/dev-server',
        './src/index.js'
      ]
    },
    output: {
      filename: '[name].js',
      path: './dist/local/',
      publicPath: '/'
    },
    module: { loaders: loaders },
    plugins: plugins
  };

})();
