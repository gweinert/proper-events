module.exports = (() => {

  "use strict";

  const loaders = require('./../config/dev-loaders.js');
  const plugins = require('./../config/plugins.js');

  return {
    entry: { main: [ './src/index.js' ] },
    output: {
      filename: '[name].[hash].js',
      path: './dist/dev/',
      publicPath: './'
    },
    module: { loaders: loaders },
    plugins: plugins
  };

})();
