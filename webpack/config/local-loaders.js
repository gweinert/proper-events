module.exports = (() => {

  "use strict";

  const pathRewriterPlugin = require('webpack-path-rewriter');

  return [
    { test: /\.jade$/, loader: pathRewriterPlugin.rewriteAndEmit({ name: '[name].html', loader: ['jade-html?pretty=true&name=main&local=true'] })},
    { test: /\.json$/, loader: 'json' },
    { test: /\.jsx?$/, loaders: ['react-hot', 'babel?presets[]=es2015,presets[]=stage-0,presets[]=react'], exclude: /node_modules/ },
    { test: /\.scss$/, loader: 'style!css!autoprefixer!sass!import-glob-loader' },
    { test: /\.(jpe?g|png|gif)$/i, loader: 'file?name=[name].[ext]!image-webpack?optimizationLevel=3' },
    { test: /\.ttf$/, loader: 'file?name=[name].[ext]' },
    { test: /\.eot$/, loader: 'file?name=[name].[ext]' },
    { test: /\.svg$/, loader: 'file?name=[name].[ext]' },
    { test: /\.woff2?$/, loader: 'file?name=[name].[ext]' }
  ];

})();
