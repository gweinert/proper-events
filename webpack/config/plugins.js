module.exports = (() => {

  "use strict";

  const argv               = require('minimist')(process.argv.slice(2));
  const packageJSON        = require('./../../package.json');
  const extractTextPlugin  = require("extract-text-webpack-plugin");
  const openBrowserPlugin  = require('open-browser-webpack-plugin');
  const pathRewriterPlugin = require('webpack-path-rewriter');
  const webpack            = require('webpack');

  const config = Object.assign({}, packageJSON.config, {
    'ENV': JSON.stringify(argv.ENV)
  });

  return [
    new extractTextPlugin('[name].[hash].css'),
    new openBrowserPlugin({ url: 'http://localhost:3000/' }),
    new pathRewriterPlugin(),
    new webpack.DefinePlugin(config),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.ProvidePlugin({
      $: 'jquery',
      _: 'lodash',
      React: 'react',
      ReactDOM: 'react-dom'
    })
  ];

})();
