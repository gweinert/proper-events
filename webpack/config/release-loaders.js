module.exports = (() => {

  "use strict";

  const extractTextPlugin  = require("extract-text-webpack-plugin");
  const pathRewriterPlugin = require('webpack-path-rewriter');

  return [
    { test: /\.jade$/, loader: pathRewriterPlugin.rewriteAndEmit({ name: '[name].html', loader: ['jade-html?pretty=true&name=main.*&local=false'] })},
    { test: /\.json$/, loader: 'json' },
    { test: /\.jsx?$/, loaders: ['babel?presets[]=es2015,presets[]=stage-0,presets[]=react'], exclude: /node_modules/ },
    { test: /\.scss$/, loader: extractTextPlugin.extract('style', 'css!autoprefixer!sass!import-glob-loader')},
    { test: /\.(jpe?g|png|gif)$/i, loader: 'file?name=[name].[hash].[ext]!image-webpack?optimizationLevel=3'},
    { test: /\.ttf$/, loader: 'file?name=[name].[hash].[ext]' },
    { test: /\.eot$/, loader: 'file?name=[name].[hash].[ext]' },
    { test: /\.svg$/, loader: 'file?name=[name].[hash].[ext]' },
    { test: /\.woff2?$/, loader: 'file?name=[name].[hash].[ext]' }
  ];

})();
