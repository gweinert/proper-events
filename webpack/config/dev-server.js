module.exports = (() => {

  return {
    contentBase: './dist/local/',
    port: 3000,
    host: 'localhost',
    hot: true,
    inline: true,
    historyApiFallback: true,
    stats: { colors: true },
    watchOptions: {
      poll: true,
      aggregateTimeout: 300
    }
  };

})();
