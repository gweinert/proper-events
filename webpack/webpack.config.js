module.exports = (() => {

  "use strict";

  const argv = require('minimist')(process.argv.slice(2));
  const dev = require('./environments/dev.js');
  const local = require('./environments/local.js');
  const release = require('./environments/release.js');

  switch (argv.ENV) {
    case 'dev': return dev;
    case 'local': return local;
    default: return release;
  }

})();
