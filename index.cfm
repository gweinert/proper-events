<cfparam name="meta" default=""/>
<cfparam name="css" default=""/>
<cfparam name="ga" default=""/>
<cfparam name="bootstrap" default=""/>
<cfparam name="js" default=""/>

<cfinclude template="/include/functions.cfm" />
<cfinclude template="/include/meta.cfm" />
<cfinclude template="/include/css.cfm" />
<cfinclude template="/include/ga.cfm" />
<cfinclude template="/include/bootstrap.cfm" />
<cfinclude template="/include/js.cfm" />

<cfsavecontent variable="index">
  <cfoutput>
    <cfinclude template="/dist/release/index.html" />
  </cfoutput>
</cfsavecontent>

<cfscript>

index = REReplace(index, "<!--meta-->(.*)<!--meta-->", meta);
index = REReplace(index, "<!--css-->(.*)<!--css-->", css);
index = REReplace(index, "<!--ga-->(.*)<!--ga-->", ga);
index = REReplace(index, "<!--bootstrap-->(.*)<!--bootstrap-->", bootstrap);
index = REReplace(index, "<!--js-->(.*)<!--js-->", js);

</cfscript>

<cfoutput>

#index#

</cfoutput>
