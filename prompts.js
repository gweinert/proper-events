(() => {
  "use strict";

  const fs = require('fs');
  const prompt = require('readline-sync');
  const json = require('./package.json');

  const prompts = [{
    id: 'author',
    questions: [{
      default: 'IdeaWork Developer',
      text: 'What is your name?'
    }]
  },{
    id: 'name',
    questions: [{
      default: 'ideawork-project',
      text: 'What is your project\'s repo name?'
    }]
  },{
    id: 'description',
    questions: [{
      default: 'IdeaWork Project',
      text: 'What is your project\'s description?'
    }]
  },{
    id: 'config',
    questions: [{
      id: 'API_PATH',
      default: 'http://project.ideaworkdev.com/api/',
      text: 'What is your project\'s API path?'
    },{
      id: 'CONTENT_DIR',
      default: 'http://project.ideaworkdev.com/content/',
      text: 'What is your project\'s content directory path?'
    }]
  }];

  console.log('\n');

  prompts.forEach((p, i) => {
    p.questions.forEach((q, j) => {
      let questionText = `${ ( i + j ) + 1 }. ${q.text} (${q.default}) `;
      let answer = prompt.question(questionText, {
        defaultInput: q.default
      });
      if (typeof json[p.id] === 'object') {
        if (answer.indexOf('.com') !== -1) {
          answer = answer.slice(-1) === '/' ?  answer : `${answer}/`;
        }
        json[p.id][q.id] = JSON.stringify(answer);
      } else { json[p.id] = answer; }
    });
  });

  fs.writeFileSync('./package.json', JSON.stringify(json, null, 2));
})();
