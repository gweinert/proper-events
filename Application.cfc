<cfcomponent displayname="Application" output="true" hint="Handle the application.">

<!--- add project name - (subdomain prefix) --->
<cfset THIS.siteStr = "propersales" />
<cfset THIS.Name = THIS.siteStr & "User" />
<cfset THIS.datasource = THIS.siteStr & "DSN" />
<cfset THIS.ApplicationTimeout = CreateTimeSpan(0,1,0,0) />
<cfset THIS.sessionTimeout = CreateTimeSpan(0,1,0,0) />
<cfset THIS.clientManagement = true />
<cfset THIS.SessionManagement = true />
<cfset THIS.SetClientCookies = false />
<cfset THIS.setDomainCookies = false />

</cfcomponent>
